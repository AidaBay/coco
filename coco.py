#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
import sys
import re
import random
import threading
import logging
import time
import pinylib
import update
from ytdl import download
from api import auto_url, soundcloud, youtube, lastfm, privacy_settings, other_apis

author = ''
repository = ''
__version__ = '0.3.8 (Pinybot v1.4.1)'
build_name = '>build names kek'
log = logging.getLogger(__name__)
CONFIG_FILE_NAME = '/config.ini'
CURRENT_PATH = sys.path[0]
CONFIG_PATH = CURRENT_PATH + CONFIG_FILE_NAME
CONFIG = pinylib.fh.configuration_loader(CONFIG_PATH)

if CONFIG is None:
    print('No file named ' + CONFIG_FILE_NAME + ' found in: ' + CONFIG_PATH)
    sys.exit(1)

def cls():
    if os.name == 'nt':
        type_clear = 'cls'
    else:
        type_clear = 'clear'
    os.system(type_clear)

if CONFIG['ascii_chars']:
    ascii_dict = pinylib.fh.ascii_loader(CONFIG['path'] + CONFIG['ascii_file'])
    if ascii_dict is None:
        CONFIG['ascii_chars'] = False
        print('No ', CONFIG['ascii_file'], ' was not found at: ', CONFIG['path'])
        print('As a result, ASCII was not loaded. Please check your settings.\n')

special_unicode = {
    'time': u"\u231A",
    'musical_note': u"\u266B",
    'indicate': u"\u261B",
    'state': u"\u21DB",
    'black_star': u"\u2605",
    'white_star': u"\u2606",
    'check_mark': u"\u2713",
    'cross_mark': u"\u2717",
    'black_heart': u"\u2764",
    'white_heart': u"\u2661",
    'toxic': u"\u2620",
    'pencil': u"\u270E",
    'scissors': u"\u2704",
    'no_width': u"\u200B",
}

# TODO move these fucntions to their own files
def eightball():
    """
    Magic eight ball.
    :return: a random answer str
    """
    answers = ['It is certain.', 'It is decidedly so.', 'without a doubt', 'Yes - definitely.',
               'You may rely on it.', 'As I see it, yes.', 'Most likely.', 'Outlook good.', 'Yes.', 'Signs point to yes.',
               'Reply hazy, try again.', 'Ask again later.', 'Better not tell you now.', 'Cannot predict now.',
               'Concentrate and ask again.', 'Don\'t count on it.', 'My reply is no', 'My sources say no.',
               'Outlook not so good.', 'Very doubtful.', 'I\'m pretty sure it\'s right.', 'You can count on it.',
               'Yes, in due time.', 'My sources say no.', 'Definitely not.', 'You will have to wait.', 'I have my doubts.',
               'Outlook so so.', 'Looks good to me!', 'Who know\'s?', 'Looking good!', 'Probably.', 'Are you kidding me?', 
               'Go for it!', 'Don\'t bet on it.', 'Forget about it.']
    return random.choice(answers)


def rate():
    """
    Rate someone
    :return: a random rate
    """
    rates = ['Ugly/10', '1/10', '2/10', '3/10', '4/10', '5/10', '6/10', '7/10',
             '8/10', '9/10', '10/10; What a babe!', '0.4/10 Take a fucking shower.',
             'Halloween is over, you can take that ogre mask off.', 'Ogre/10', 'LOL That thing?!',
             'Not bad looking.', 'Would bang/10', 'gr8 m8 r8 8/8', 'Mom probably dropped them on their head',
             'Would bang/10 (If I was drunk)', "Aida would probably hit it, that's not saying much though.",
             "If I had a penis, you would see a tent right now", "How about I rate you?", "Leave me alone.",
             "Either their cam is dirty or they need proactive.", "Can smell them from here. :'( ",
             "I would rather die than rate that ugly hoe", "A Silverback Gorilla seems more appealing",
             "Why is this person even alive?", "Rating people? Seriously? Are you 15 or what?",
             "Moist worthy", "Fap worthy", "Where?", "Oooh, that person... Who?", "Rape/10",
             "Someone grab lube, I'm going in.", "Aida/10", "Oh God... Do I have to?", "Sometimes I wonder why you even ask...",
             "When in doubt, fap it out", "Depressingly Fappable",
             "Can't you just rate me instead?", "I want to touch it... softly...", "Can't stop looking at them.",
             "Eyes that are as beautiful as an ocean sunset and skin that are as smooth as the surface of a baby's butt.",
             "When am I allowed to take a sneakshot and fap to them in my free time?",
             "Holy hot, do I even need to rate?", "I swear to God, if you make me rate again, I will force a shutdown and you will never see me for another "
             "24 hours.. But 8/10 doe",
             "I thought they were a celebrity...", "I'd rather eat aida's asshole.", "Too awesome to be real",
             "I wanna secretly cuddle with them while they are asleep so I can just appreciate all of them while slowly erecting a boner.",
             "Oh, I didn't even know they were in the room.", "That nigga needs to wash their hair.",
             "Sometimes I fantasize about them... getting ripped apart by Hulk Hogan's muscular asscheeks.",
             "If I was human, I'd have fallen in love. With a slight chance of an erection.",
             "You'll be my curry, I'll be your rice.",
             "Akon would make a song about them", "Looks like the same thing that comes out of my virtual anus",
             "I think PSY made a song about them... I think it was called Daddy?",
             'CUNT RADAR IS OFF THE CHARTS ON THIS ONE!',
             "I think it crawled out of the sewers.", "If it was opposite day, they'd be ugly as fuck.",
             "Another rate, another waste of time.", 'Maybe a 2 at best.',
             'I dont fucking know. Dont ask dumb shit.',
             "This just in, another god-like being has came down from the heavens to awe us all",
             "Moderately tempted to staple my eyes shut", "Quick! Someone hold my dick down!",
             "I came in, saw them, left.", "My virtual heart just exploded",
             "As Kawaii as a nice peice of fresh turd",
             "When is it okay to vomit?", "Maybe would bang/10", "Needs to be cuddled with.",
             "Smells like sweat and crusty socks", "My nipples just hardened",
             "I'm fine with jailbait if it's them ;)",
             "They're like watching an old woman's breasts wrinkle as time passes by",
             "My dick just grew 2 more inches", "It looks like a saggy ballsack",
             'Lexie would probably fuck em. Not saying much though.',
             'Aida should go kill himself.', 'This is stupid.', '- left the room.',
             '- changed nickname to BotOnStrike']
    return random.choice(rates)


def give_drug():
    drugs = ['Gives you meth, you is a fucking tweaker.', 'Gives you bad LSD. Enjoy the trip ;3',
             'Gives you peyote. "Holy fuck I am the earth!".', 'Gives you ecstacy... sadly you overdoses...  :(',
             'Gives you weed, smoke weed erryday!', 'Gives you AIDS wait that isn\'t a drug still counts though!',
             'Gives you morphine. MIGHTY MORPHINE POWER RANGERS GO',
             'Gives you a dead kid\'s body full of coke, With love- MS13',
             'Injects krokodil into your dick. You\'ve met with a terrible fate....',
             'Gives you Charlie Sheen. Sadly your head explodes and your children weep over your exploded body!',
             'Gives you herpes. enjoy']
    return random.choice(drugs)


class TinychatBot(pinylib.TinychatRTMPClient):
    init_time = pinylib.time.time()
    key = CONFIG['key']
    botters = []
    if not os.path.exists(CONFIG['path'] + CONFIG['botteraccounts']):
        open(CONFIG['path'] + CONFIG['botteraccounts'], mode='w')
    botteraccounts = pinylib.fh.file_reader(CONFIG['path'], CONFIG['botteraccounts'])
    if not os.path.exists(CONFIG['path'] + CONFIG['autoforgive']):
        open(CONFIG['path'] + CONFIG['autoforgive'], mode='w')
    autoforgive = pinylib.fh.file_reader(CONFIG['path'], CONFIG['autoforgive'])
    yt_type = 'youTube'
    sc_type = 'soundCloud'
    is_mod_playing = False
    media_timer_thread = None
    media_start_time = 0
    inowplay = 0
    playlist_mode = CONFIG['playlist_mode']
    playlist = []
    search_play_lists = []
    search_list = []
    last_played_media = {}
    last_yt = ''
    privacy_settings = object
    no_cam = CONFIG['no_cam']
    no_guests = CONFIG['no_guests']
    auto_url_mode = CONFIG['auto_url_mode']
    last_url = ''
    cam_blocked = []
    bot_listen = True
    forgive_all = False
    syncing = False
    pmming_all = False
    snap_line = 'I just took a video snapshot of this chatroom. Check it out here:'
    lltc_mode = False
    
    def on_join(self, join_info_dict):
        log.info('User join info: %s' % join_info_dict)
        user = self.add_user_info(join_info_dict['nick'])
        user.nick = join_info_dict['nick']
        user.user_account = join_info_dict['account']
        user.id = join_info_dict['id']
        user.is_mod = join_info_dict['mod']
        user.is_owner = join_info_dict['own']
        if join_info_dict['account']:
            tc_info = pinylib.tinychat_api.tinychat_user_info(join_info_dict['account'])
            if tc_info is not None:
                user.tinychat_id = tc_info['tinychat_id']
                user.last_login = tc_info['last_active']
            if join_info_dict['own']:
                self.console_write(pinylib.COLOR['red'], '[joins] Room Owner %s:%d:%s' % (join_info_dict['nick'],
                                   join_info_dict['id'], join_info_dict['account']))
            elif join_info_dict['mod']:
                self.console_write(pinylib.COLOR['bright_red'], '[joins] Moderator %s:%d:%s' % (join_info_dict['nick'],
                                   join_info_dict['id'], join_info_dict['account']))
            else:
                self.console_write(pinylib.COLOR['bright_yellow'], '[joins] %s:%d has account: %s' % (join_info_dict['nick'],
                                   join_info_dict['id'], join_info_dict['account']))

                badaccounts = pinylib.fh.file_reader(CONFIG['path'], CONFIG['badaccounts'])
                if badaccounts is not None:
                    if join_info_dict['account'] in badaccounts:
                        if self.is_client_mod:
                            self.send_ban_msg(join_info_dict['nick'], join_info_dict['id'])
        else:
            if join_info_dict['id'] is not self.client_id:
                if self.no_guests:
                    self.send_ban_msg(join_info_dict['nick'], join_info_dict['id'])
                else:
                    self.console_write(pinylib.COLOR['bright_cyan'], '[joins] %s:%d joined the room.' %
                                       (join_info_dict['nick'], join_info_dict['id']))

    def on_joinsdone(self):
        if not self.is_reconnected:
            if CONFIG['auto_message_enabled']:
                self.start_auto_msg_timer()
        if self.is_client_mod:
            self.send_banlist_msg()
        if self.is_client_owner and self._roomtype != 'default':
            threading.Thread(target=self.get_privacy_settings).start()

    def on_avon(self, uid, name):
        if self.no_cam or name in self.cam_blocked:
            self.send_close_user_msg(name)
        else:
            user = self.find_user_info(name)

            if not user.is_owner or not user.is_mod or not user.has_power:
                uid_parts = str(uid).split(':')
                if len(uid_parts) is 2:
                    clean_uid = uid_parts[0]
                    user_device = u'' + uid_parts[1]
                    user.device_type = user_device
                    if user_device == 'android':
                        self.console_write(pinylib.COLOR['cyan'],
                                           '[users] %s:%s is broadcasting from an android device.' % (name, clean_uid))
                    elif user_device == 'ios':
                        self.console_write(pinylib.COLOR['cyan'],
                                           '[users] %s:%s is broadcasting from an ios device.' % (name, clean_uid))
                    else:
                        self.console_write(pinylib.COLOR['cyan'],
                                           '[users] %s:%s is broadcasting from an unknown device.' % (name, clean_uid))

                if CONFIG['auto_close']:
                    if name.startswith('newuser'):
                        self.send_close_user_msg(name)

                    elif name.startswith('guest-'):
                        self.send_close_user_msg(name)

                    elif len(user.device_type) is not 0:
                        if CONFIG['ban_mobiles']:
                            self.send_ban_msg(name, uid)
                            # Remove next line to keep ban.
                            self.send_forgive_msg(uid)
                        else:
                            self.send_close_user_msg(name)
                    return

            if len(CONFIG['welcome_broadcast_msg']) > 0:
                if len(name) is 2:
                    self.send_bot_msg(CONFIG['welcome_broadcast_msg'] + ' *' + name + '*', self.is_client_mod)

            self.console_write(pinylib.COLOR['cyan'], '[users] %s:%s is broadcasting.' % (name, uid))

    def auto_pm(self, nickname):
        pm_msg = CONFIG['pm_msg']

        # TODO: Add custom allocations for 'replacement variables'.
        if '%user%' in pm_msg:
            pm_msg = pm_msg.replace('%user%', nickname)
        if '%room%' in pm_msg:
            pm_msg = pm_msg.replace('%room%', self.roomname.upper())

        if '|' in pm_msg:
            message_parts = pm_msg.split('|')
            for x in xrange(len(message_parts)):
                self.send_private_msg(message_parts[x], nickname)
        else:
            self.send_private_msg(pm_msg, nickname)

    def on_nick(self, old, new, uid):
        if uid is not self.client_id:
            old_info = self.find_user_info(old)
            old_info.nick = new
            if old in self.room_users.keys():
                del self.room_users[old]
                self.room_users[new] = old_info

            user = self.find_user_info(new)

            if old in self.botters:
                self.botters.remove(old)
                self.botters.append(new)

            # TODO This is on nick change instead of on join
            #      Should be timing the user's join and after X seconds of no nick change
            #      send them these messages
            if new.startswith('guest-'):
                self.send_private_msg('Your name is guest and others may not be able to view your message.', new)
                self.send_private_msg('Add a letter or number to your name to solve this.', new) 
                self.send_bot_msg(new + ' has been notified of their guest nick.', self.is_client_mod)
                return

            if "`" in new or "-" in new or "\\" in new:
                self.send_ban_msg(new, uid)
                return

            if old.startswith('guest-'):
                bn = pinylib.fh.file_reader(CONFIG['path'], CONFIG['badnicks'])

                if bn is not None and new in bn:
                    if self.is_client_mod:
                        self.send_ban_msg(new, uid)
                else:
                    if user is not None:
                        if CONFIG['welcome_user']:
                            if user.user_account:
                                self.send_bot_msg('*Welcome to *%s*:' + special_unicode['no_width'] + '%s' +
                                                  special_unicode['no_width'] % (self.roomname, user.user_account),
                                                  self.is_client_mod)
                            else:
                                self.send_bot_msg('*Welcome to* ' + self.roomname + ' *' + special_unicode['no_width'] +
                                                  new + special_unicode['no_width'] + '*', self.is_client_mod)

                    if self.media_timer_thread is not None and self.media_timer_thread.is_alive():
                        if not self.is_mod_playing:
                            self.send_media_broadcast_start(self.last_played_media['type'],
                                                            self.last_played_media['video_id'],
                                                            time_point=self.current_media_time_point(),
                                                            private_nick=new)

                    if CONFIG['auto_pm'] and len(CONFIG['pm_msg']) is not 0:
                        self.auto_pm(new)

            self.console_write(pinylib.COLOR['bright_cyan'], '[joins] %s:%s changed nick to: %s' % (old, uid, new))

    def on_kick(self, uid, name):
        if uid is not self.client_id:
            user = self.find_user_info(name)
            if user.user_account in self.autoforgive:
                self.send_forgive_msg(user.id)
                self.console_write(pinylib.COLOR['bright_red'],
                                   '[joins] %s:%s was kicked and has been forgive (autoforgive).' % (name, uid))
            else:
                self.console_write(pinylib.COLOR['bright_red'], '[joins] %s:%s was banned.' % (name, uid))
                self.send_banlist_msg()

    def on_quit(self, uid, name):
        if uid is not self.client_id:
            if name in self.room_users.keys():
                self.tidy_exit(name)
                del self.room_users[name]
                self.console_write(pinylib.COLOR['cyan'], '[joins] %s:%s left the room.' % (name, uid))

    def tidy_exit(self, name):
        user = self.find_user_info(name)
        if user.nick in self.botters:
            self.botters.remove(user.nick)
        if user.user_account:
            if user.user_account in self.botteraccounts:
                self.botteraccounts.remove(user.user_account)
        if user.nick in self.cam_blocked:
            self.cam_blocked.remove(user.nick)

    def on_reported(self, uid, nick):
        self.console_write(pinylib.COLOR['bright_red'], '[status] You were reported by %s:%s.' % (nick, uid))
        if CONFIG['bot_report_kick']:
            self.send_ban_msg(nick, uid)
            self.send_bot_msg('*Auto-Banned:* (reporting the bot)', self.is_client_mod)
            self.send_forgive_msg(uid)

    # Media Events.
    def on_media_broadcast_start(self, media_type, video_id, time_point, usr_nick):
        if self.user_obj.is_mod:
            self.is_mod_playing = True
            self.cancel_media_event_timer()

            if 'pause' in self.last_played_media:
                del self.last_played_media['pause']

            video_time = 0

            if media_type == 'youTube':
                _youtube = youtube.youtube_time(video_id, check=False)
                if _youtube is not None:
                    self.last_played_media = _youtube
                    video_time = _youtube['video_time']

            elif media_type == 'soundCloud':
                _soundcloud = soundcloud.soundcloud_track_info(video_id)
                if _soundcloud is not None:
                    self.last_played_media = _soundcloud
                    video_time = _soundcloud['video_time']

            self.media_event_timer(video_time)
            self.console_write(pinylib.COLOR['bright_magenta'], '[media] %s is playing %s %s (%s)'
                               % (usr_nick, media_type, video_id, time_point))

    def on_media_broadcast_close(self, media_type, usr_nick):
        if self.user_obj.is_mod:
            self.cancel_media_event_timer()
            if 'pause' in self.last_played_media:
                del self.last_played_media['pause']
            self.console_write(pinylib.COLOR['bright_magenta'], '[media] %s closed the %s' % (usr_nick, media_type))

    def on_media_broadcast_paused(self, media_type, usr_nick):
        if self.user_obj.is_mod:
            self.cancel_media_event_timer()
            
            if 'pause' in self.last_played_media:
                del self.last_played_media['pause']
            
            ts_now = int(pinylib.time.time() * 1000)
            self.last_played_media['pause'] = ts_now - self.media_start_time
            self.console_write(pinylib.COLOR['bright_magenta'], '[media] %s paused the %s' % (usr_nick, media_type))

    def on_media_broadcast_play(self, media_type, time_point, usr_nick):
        if self.user_obj.is_mod:
            self.cancel_media_event_timer()
            new_media_time = self.last_played_media['video_time'] - time_point
            self.media_start_time = new_media_time

            if 'pause' in self.last_played_media:
                del self.last_played_media['pause']

            self.media_event_timer(new_media_time)
            self.console_write(pinylib.COLOR['bright_magenta'], '[media] %s resumed the %s at: %s'
                               % (usr_nick, media_type, self.to_human_time(time_point)))

    def on_media_broadcast_skip(self, media_type, time_point, usr_nick):
        if self.user_obj.is_mod:
            self.cancel_media_event_timer()
            new_media_time = self.last_played_media['video_time'] - time_point
            self.media_start_time = new_media_time

            if 'pause' in self.last_played_media:
                self.last_played_media['pause'] = new_media_time

            self.media_event_timer(new_media_time)
            self.console_write(pinylib.COLOR['bright_magenta'], '[media] %s time searched the %s at: %s'
                               % (usr_nick, media_type, self.to_human_time(time_point)))

    def send_media_broadcast_start(self, media_type, video_id, time_point=0, private_nick=None):
        mbs_msg = '/mbs %s %s %s' % (media_type, video_id, time_point)
        if private_nick is not None:
            self.send_undercover_msg(private_nick, mbs_msg)
        else:
            self.is_mod_playing = False
            self.send_chat_msg(mbs_msg)

    def spam_prevention(self, msg, msg_sender):
        user = self.find_user_info(msg_sender)
        unicode_spam = (re.findall("\x85", msg))
        caps = (re.findall("[A-Z]", msg))

        if len(unicode_spam) > 1 in msg:
            self.send_ban_msg(user.nick, user.id)
            self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + user.nick + " was banned for Unicode spam")
            return

        if 'tinychat.com/' + str(self.roomname) not in msg:
            text_search = re.search(r'tinychat.com\/\w+($| |\/+ |\/+$)', msg, re.I)
            if text_search:
                self.send_ban_msg(user.nick, user.id)
                self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + user.nick + " was banned for advertising another room")
                return

        if CONFIG['snapshot']:
            if self.snap_line in msg:
                self.send_ban_msg(user.nick, user.id)
                #self.send_forgive_msg(user.id)
                self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + user.nick + " was banned for taking a snapshot")
                return

        if "http" in msg or user.is_owner or user.is_mod:
            pass
        else:
	    if len(caps) >= 45:
                self.send_ban_msg(user.nick, user.id)
                self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + user.nick + " was banned for capital characters")
                return
            else:
                pass

    # Message Method.
    def send_bot_msg(self, msg, is_mod=False):
        if is_mod:
            self.send_owner_run_msg(msg)
        else:
            self.send_chat_msg(msg)

    def message_handler(self, msg_sender, msg):
        if not self.user_obj.has_power:
            if self.user_obj.nick in self.botters:
                self.user_obj.has_power = True
            elif self.user_obj.user_account in self.botteraccounts:
                self.user_obj.has_power = True

        if not self.bot_listen:
            # TODO: This statement should be checked to see if it works or not.
            if not self.user_obj.is_owner and not self.user_obj.is_super \
                    and not self.user_obj.is_mod and not self.user_obj.has_power:
                return

        if CONFIG['spam_prevention']:
            if not self.user_obj.is_owner and not self.user_obj.is_super \
                    and not self.user_obj.is_mod and not self.user_obj.has_power:
                spam_check = threading.Thread(target=self.spam_prevention, args=(msg, msg_sender,))
                spam_check.start()
                spam_check.join()

        if self.auto_url_mode:
            threading.Thread(target=self.do_auto_url, args=(msg, )).start()

        if self.user_obj.nick.startswith('guest-'):
            self.send_undercover_msg(self.user_obj.nick, 'You are a guest. Please append a number or letter to your name.')
            self.send_private_msg('You are a guest, others may not be able to read your messages.', self.user_obj.nick)
            self.send_private_msg('Add a number or letter to your name to resolve this issue, or just refresh.', self.user_obj.nick)
            self.console_write(pinylib.COLOR['bright_yellow'], "[users] " + self.user_obj.nick + " was notified of their guest nick.")
            
        if 'youtu' in msg:
            tube = re.findall(r'(?:be\/|\?v=)(.{11})', msg)
            self.last_yt = tube[0]
            self.console_write(pinylib.COLOR['bright_magenta'], "[media] " + self.user_obj.nick + " posted the URL: " + self.last_yt)
            self.console_write(pinylib.COLOR['bright_magenta'], "[media] " + tube[0] + ' set as last_yt')
        
        if msg.startswith(CONFIG['prefix']):
            parts = msg.split(' ')
            cmd = parts[0].lower().strip()
            cmd_arg = ' '.join(parts[1:]).strip()
            cmd_args = cmd_arg.split(' ')

            if CONFIG['ascii_chars']:
                ascii_result = self.do_ascii(cmd)
                if ascii_result:
                    return

            # Super mod commands:
            if cmd == CONFIG['prefix'] + 'mod':
                threading.Thread(target=self.do_make_mod, args=(cmd_arg,)).start()

            elif cmd == CONFIG['prefix'] + 'rmod':
                threading.Thread(target=self.do_remove_mod, args=(cmd_arg,)).start()

            elif cmd == CONFIG['prefix'] + 'dir':
                threading.Thread(target=self.do_directory).start()

            elif cmd == CONFIG['prefix'] + 'p2t':
                threading.Thread(target=self.do_push2talk).start
            elif cmd == CONFIG['prefix'] + 'gr':
                threading.Thread(target=self.do_green_room).start()

            elif cmd == CONFIG['prefix'] + 'crb':
                threading.Thread(target=self.do_clear_room_bans).start()

            # Owner and super mod commands:
            elif cmd == CONFIG['prefix'] + 'kill':
                self.do_kill()
            
            elif cmd == CONFIG['prefix'] + 'sleep':
                self.do_sleep()

            elif cmd == CONFIG['prefix'] + 'reboot':
                self.do_reboot()

            elif cmd == CONFIG['prefix'] + 'spam':
                self.do_spam()

            elif cmd == CONFIG['prefix'] + 'snap':
                self.do_snapshot()

            elif cmd == CONFIG['prefix'] + 'camblock':
                self.do_camblock(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'autoclose':
                self.do_autoclose()

            elif cmd == CONFIG['prefix'] + 'mobiles':
                self.do_ban_mobiles()

            elif cmd == CONFIG['prefix'] + 'autourl':
                self.do_auto_url_mode()

            elif cmd == CONFIG['prefix'] + 'playlist':
                self.do_playlist_mode()

            # TODO: Make sure enabling guests/newusers bans/kicks the respective users in the room as well.
            elif cmd == CONFIG['prefix'] + 'guests':
                self.do_guest_nick_ban()

            elif cmd == CONFIG['prefix'] + 'newuser':
                self.do_newuser_user_ban()

            elif cmd == CONFIG['prefix'] + 'mute':
                threading.Thread(target=self.do_mute).start()

            elif cmd == CONFIG['prefix'] + 'p2tnow':
                self.do_instant_push2talk()

            elif cmd == CONFIG['prefix'] + 'autopm':
                self.do_auto_pm()

            elif cmd == CONFIG['prefix'] + 'privateroom':
                self.do_private_room()

            #elif cmd == CONFIG['prefix'] + 'botter':
            #    threading.Thread(target=self.do_botter, args=(cmd_arg, )).start()
            elif cmd == CONFIG['prefix'] + 'op':
                threading.Thread(target=self.do_op, args=(cmd_arg, )).start()
           
            elif cmd == CONFIG['prefix'] + 'deop':
                search_id = search.split('/')[3]
                threading.Thread(target=self.do_deop, args=(cmd_arg, )).start()
            
            elif cmd == CONFIG['prefix'] + 'protect':
                threading.Thread(target=self.do_autoforgive, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'close':
                self.do_close_broadcast(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'clr':
                self.do_clear()

            elif cmd == CONFIG['prefix'] + 'media':
                self.do_media_info()

            # - Higher-level commands:
            elif cmd == CONFIG['prefix'] + 'topicis':
                threading.Thread(target=self.do_current_topic).start()

            elif cmd == CONFIG['prefix'] + 'topic':
                self.do_topic(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'kick':
                self.do_kick(cmd_arg, False)

            elif cmd == CONFIG['prefix'] + 'ban':
                self.do_kick(cmd_arg, True)

            elif cmd == CONFIG['prefix'] + 'forgive':
                threading.Thread(target=self.do_forgive, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'bn':
                threading.Thread(target=self.do_bad_nick, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'rmbn':
                self.do_remove_bad_nick(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'bs':
                threading.Thread(target=self.do_bad_string, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'rmbs':
                self.do_remove_bad_string(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'ba':
                threading.Thread(target=self.do_bad_account, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'rmba':
                self.do_remove_bad_account(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'list':
                self.do_list_info(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'uinfo':
                threading.Thread(target=self.do_user_info, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'blockyt':
                threading.Thread(target=self.do_botter, args=(cmd_arg, )).start()

            # Standard media commands:
            elif cmd == CONFIG['prefix'] + 'yt':
                threading.Thread(target=self.do_play_media, args=(self.yt_type, cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'sc':
                threading.Thread(target=self.do_play_media, args=(self.sc_type, cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'syncall':
                threading.Thread(target=self.do_sync_media).start()

            elif cmd == CONFIG['prefix'] + 'syt':
                threading.Thread(target=self.do_youtube_search, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'psyt':
                self.do_play_youtube_search(cmd_arg)

            # Specific media control commands:
            # TODO: Requires new media handling class.

            elif cmd == CONFIG['prefix'] + 'replay':
                self.do_media_replay()

            elif cmd == CONFIG['prefix'] + 'skip':
                self.do_skip()

            elif cmd == CONFIG['prefix'] + 'stop':
                self.do_close_media()

            # TODO: Bug present; [collect info & state]
            # elif cmd == CONFIG['prefix'] + 'pause':
            # self.do_pause_media()

            # TODO: Bug present; [collect info & state]
            # elif cmd == CONFIG['prefix'] + 'resume':
            # self.do_resume_media()

            # TODO: Make sure the time is correctly updated in the background media timer,
            #       so newly joined users receive the correct time.
            # elif cmd == CONFIG['prefix'] + 'seek':
            #     self.do_seek_media(cmd_arg)

            # Playlist media commands:
            elif cmd == CONFIG['prefix'] + 'pl':
                threading.Thread(target=self.do_youtube_playlist_videos, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'plsh':
                threading.Thread(target=self.do_youtube_playlist_search, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'pladd':
                threading.Thread(target=self.do_youtube_playlist_search_choice, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'top40':
                threading.Thread(target=self.do_charts).start()

            elif cmd == CONFIG['prefix'] + 'top':
                threading.Thread(target=self.do_lastfm_chart, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'ran':
                threading.Thread(target=self.do_lastfm_random_tunes, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'tag':
                threading.Thread(target=self.search_lastfm_by_tag, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'rm':
                self.do_delete_playlist_item(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'cpl':
                self.do_clear_playlist()

            # Public commands:
            #elif cmd == CONFIG['prefix'] + 'v':
            #    threading.Thread(target=self.do_version).start()


            elif cmd == CONFIG['prefix'] + 'now':
                threading.Thread(target=self.do_now_playing).start()

            elif cmd == CONFIG['prefix'] + 'next':
                threading.Thread(target=self.do_next_tune_in_playlist).start()

            elif cmd == CONFIG['prefix'] + 'pls':
                threading.Thread(target=self.do_playlist_status).start()

            elif cmd == CONFIG['prefix'] + 'uptime':
                self.do_uptime()

            elif cmd == CONFIG['prefix'] + 'pmme':
                self.do_pmme()

            # - Private media commands:
            elif cmd == CONFIG['prefix'] + 'ytuser':
                threading.Thread(target=self.do_play_private_media, args=(self.yt_type, cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'scme':
                threading.Thread(target=self.do_play_private_media, args=(self.sc_type, cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'syncme':
                self.do_sync_media_user()

            elif cmd == CONFIG['prefix'] + 'stopme':
                self.do_stop_private_media()
            
            # TODO: Decide if more private media functions are required.
            # [Private pause/rewind/seek procedures here (?)]

            # API commands:
            # - Tinychat API commands:
            elif cmd == CONFIG['prefix'] + 'spy':
                threading.Thread(target=self.do_spy, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'acspy':
                threading.Thread(target=self.do_account_spy, args=(cmd_arg, )).start()

            # - External API commands:
            elif cmd == CONFIG['prefix'] + 'urb':
                threading.Thread(target=self.do_search_urban_dictionary, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'wea':
                threading.Thread(target=self.do_weather_search, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'ip':
                threading.Thread(target=self.do_whois_ip, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'ddg':
                threading.Thread(target=self.do_duckduckgo_search, args=(cmd_arg, )).start()

            # TODO: Fix bug in wikipedia parsing.
            elif cmd == CONFIG['prefix'] + 'wiki':
                threading.Thread(target=self.do_wiki_search, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'imdb':
                threading.Thread(target=self.do_omdb_search, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'ety':
                threading.Thread(target=self.do_etymonline_search, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'define':
                threading.Thread(target=self.do_dict, args=(cmd_arg, )).start()
            
            # Entertainment/alternative media commands:
            elif cmd == CONFIG['prefix'] + 'cn':
                threading.Thread(target=self.do_chuck_norris).start()

            elif cmd == CONFIG['prefix'] + '8ball':
                self.do_8ball(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'yomama':
                threading.Thread(target=self.do_yo_mama_joke).start()

            elif cmd == CONFIG['prefix'] + 'advice':
                threading.Thread(target=self.do_advice).start()

            elif cmd == CONFIG['prefix'] + 'joke':
                threading.Thread(target=self.do_one_liner, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'time':
                threading.Thread(target=self.do_time, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'time+':
                threading.Thread(target=self.do_time_additional, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'up':
                threading.Thread(target=self.do_cam_up).start()

            elif cmd == CONFIG['prefix'] + 'down':
                threading.Thread(target=self.do_cam_down).start()
            elif cmd == CONFIG['prefix'] + 'rate':
                self.do_rate(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'ddos':
                self.do_ddos(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'jew':
                threading.Thread(target=self.do_jew, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'fake':
                threading.Thread(target=self.do_fake, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'drug':
                self.do_drug(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'tip':
                self.do_tip(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'trump':
                self.do_trump(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'savage':
                self.do_savage(cmd_arg)

            elif cmd == CONFIG['prefix'] + 'catgif':
                self.do_cat()

            elif cmd == CONFIG['prefix'] + 'download':
                threading.Thread(target=self.do_download, args=(cmd_arg, )).start()

            elif cmd == CONFIG['prefix'] + 'age':
                threading.Thread(target=self.do_age, args=(cmd_arg, )).start()
          
            elif cmd == CONFIG['prefix'] + 'help':
                self.do_help()
            
            elif cmd == CONFIG['prefix'] + 'stfu':
                self.do_stfu()

            elif cmd == CONFIG['prefix'] + '0z':
                self.do_oz()

            elif cmd == CONFIG['prefix'] + 'fixme':
                threading.Thread(target=self.do_fixme, args=(cmd_arg, )).start()
            
            elif cmd == CONFIG['prefix'] + 'ts':
                self.send_bot_msg('*Teamspeak3 address is:* programmerfag.com   *Feel free to join :D*', self.is_client_mod)
            
            self.console_write(pinylib.COLOR['bright_yellow'], '[users] ' + 
                    self.user_obj.nick + ': ' + cmd + ' ' + cmd_arg)

        else:
            self.console_write(pinylib.COLOR['white'], self.user_obj.nick + ': ' + msg)
            if self.is_client_mod and not self.user_obj.is_owner and not self.user_obj.is_super and not \
                    self.user_obj.is_mod and not self.user_obj.has_power:
                threading.Thread(target=self.check_msg_for_bad_string, args=(msg, )).start()
                
        self.user_obj.last_msg = msg

    # == Super Mod Commands Methods. ==
    def do_make_mod(self, account):
        if self.is_client_owner:
            if self.user_obj.is_super:
                if len(account) is 0:
                    self.send_bot_msg('*Missing account name.*', self.is_client_mod)
                else:
                    tc_user = self.privacy_settings.make_moderator(account)
                    if tc_user is None:
                        self.send_bot_msg('*The account is invalid.*', self.is_client_mod)
                    elif tc_user:
                        self.send_bot_msg('*' + account + ' was made a room moderator.*', self.is_client_mod)
                    elif not tc_user:
                        self.send_bot_msg('*The account is already a moderator.*', self.is_client_mod)

    def do_remove_mod(self, account):
        if self.is_client_owner:
            if self.user_obj.is_super:
                if len(account) is 0:
                    self.send_bot_msg('*Missing account name.*', self.is_client_mod)
                else:
                    tc_user = self.privacy_settings.remove_moderator(account)
                    if tc_user:
                        self.send_bot_msg('*' + account + ' is no longer a room moderator.*', self.is_client_mod)
                    elif not tc_user:
                        self.send_bot_msg('*' + account + ' is not a room moderator.*', self.is_client_mod)

    def do_directory(self):
        if self.is_client_owner:
            if self.user_obj.is_super:
                if self.privacy_settings.show_on_directory():
                    self.send_bot_msg('*Room IS shown on the directory.*', self.is_client_mod)
                else:
                    self.send_bot_msg('*Room is NOT shown on the directory.*', self.is_client_mod)

    def do_push2talk(self):
        if self.is_client_owner:
            if self.user_obj.is_super:
                if self.privacy_settings.set_push2talk():
                    self.send_bot_msg('*Push2Talk is enabled.*', self.is_client_mod)
                else:
                    self.send_bot_msg('*Push2Talk is disabled.*', self.is_client_mod)

    def do_green_room(self):
        if self.is_client_owner:
            if self.user_obj.is_super:
                if self.privacy_settings.set_greenroom():
                    self.send_bot_msg('*Green room is enabled.*', self.is_client_mod)
                else:
                    self.send_bot_msg('*Green room is disabled.*', self.is_client_mod)

    def do_clear_room_bans(self):
        if self.is_client_owner:
            if self.user_obj.is_super:
                if self.privacy_settings.clear_bans():
                    self.send_bot_msg('*All room bans was cleared.*', self.is_client_mod)

    # == Owner And Super Mod Command Methods. ==
    def do_kill(self):
        if self.user_obj.is_owner or self.user_obj.is_super:
            threading.Thread(target=self.disconnect).start()
            self.console_write(pinylib.COLOR['bright_magenta'], "[core] " + self.user_obj.nick + " killed the bot")
            sys.exit(0)

    def do_lltc(self, passcode):
        # TODO move to config so it's not in the public repo >__<
        if passcode == '0z is gay':
            if self.lltc_mode is False:
                self.lltc_mode = True
                self.send_bot_msg('LLTC mode enabled', self.is_client_mod)
                print(self.lltc_mode)
            else:
                self.lltc_mode = False
                print(self.lltc_mode)
                self.send_media_broadcast_start(self.yt_type, 'vQQTlDvI3-o')
                self.send_bot_msg('LLTC mode disabled', self.is_client_mod)
            while self.lltc_mode is True:    
                self.send_media_broadcast_start(self.yt_type, 'H_AQFnqMY3E')
                threading.Thread(target=pinylib.time.sleep(176))
        else:
            self.send_private_msg('Wrong passcode m8.', self.user_obj.nick)
    
    def do_reboot(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod:
            self.reconnect()
            self.console_write(pinylib.COLOR['bright_magenta'], "[core] " + self.user_obj.nick + " reconnected the bot")

    def do_media_info(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                self.send_owner_run_msg('*I Now Play:* ' + str(self.inowplay))
                self.send_owner_run_msg('*Playlist Length:* ' + str(len(self.playlist)))
                self.send_owner_run_msg('*Current Time Point:* ' + self.to_human_time(self.current_media_time_point()))
                self.send_owner_run_msg('*Active Threads:* ' + str(threading.active_count()))
            self.console_write(pinylib.COLOR['bright_magenta'], "[core] " + self.user_obj.nick + " asked for media info")

    def do_sleep(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            self.bot_listen = not self.bot_listen
            self.send_bot_msg('*Bot listening set to*: *' + str(self.bot_listen) + '*', self.is_client_mod)
            self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + " has changed the sleep state: " + self.bot_listen)

    def do_spam(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            CONFIG['spam_prevention'] = not CONFIG['spam_prevention']
            self.send_bot_msg('*Text Spam Prevention*: *' + str(CONFIG['spam_prevention']) + '*', self.is_client_mod)
            self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + " set spam_prevention to " + CONFIG['spam_prevention'])

    def do_snapshot(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            CONFIG['snapshot'] = not CONFIG['snapshot']
            self.send_bot_msg('*Snapshot Prevention*: *' + str(CONFIG['snapshot']) + '*', self.is_client_mod)
            self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + " set snapshots to " + CONFIG['snapshot'])

    def do_autoclose(self):
        """ Toggles autoclose. """
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            CONFIG['auto_close'] = not CONFIG['auto_close']
            self.send_bot_msg('*Auto closing mobiles/guests/newusers*: *' + str(CONFIG['auto_close']) + '*',
                              self.is_client_mod)
            self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + " set auto_close to " + CONFIG['auto_close'])

    def do_ban_mobiles(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            CONFIG['ban_mobiles'] = not CONFIG['ban_mobiles']
            self.send_bot_msg('*Banning mobile users on cam*: *' + str(CONFIG['ban_mobiles']) + '*', self.is_client_mod)
            self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + " set ban_mobiles to " + CONFIG['ban_mobiles'])

    def do_auto_url_mode(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            self.auto_url_mode = not self.auto_url_mode
            self.send_bot_msg('*Auto-Url Mode*: *' + str(self.auto_url_mode) + '*', self.is_client_mod)
            self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + " set auto_url to " + self.auto_url_mode)

    def do_playlist_mode(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            self.playlist_mode = not self.playlist_mode
            self.send_bot_msg('*Playlist Mode: *' + str(self.playlist_mode), self.is_client_mod)
            self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + " set playlist_mode to " + self.playlist_mode)

    def do_guest_nick_ban(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            CONFIG['guest_nick_ban'] = not CONFIG['guest_nick_ban']
            self.send_bot_msg('*Banning "guests-"*: *' + str(CONFIG['guest_nick_ban']) + '*', self.is_client_mod)
            self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + " set guest_nick_ban to " + 
                                CONFIG['guest_nick_ban'])

    def do_newuser_user_ban(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            CONFIG['new_user_ban'] = not CONFIG['new_user_ban']
            self.send_bot_msg('*Newuser banning*: *' + str(CONFIG['new_user_ban']) + '*', self.is_client_mod)
            self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + " set new_user_ban to " + 
                                CONFIG['new_user_ban'])

    def do_camblock(self, on_block):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if len(on_block) is 0:
                self.send_bot_msg('Please state a user to cam block.', self.is_client_mod)
            else:
                user = self.find_user_info(on_block)
                if user is not None:
                    if user.nick not in self.cam_blocked:
                        self.cam_blocked.append(user.nick)
                        self.send_close_user_msg(user.nick)
                        self.send_bot_msg('*' + user.nick + '* is now cam blocked.', self.is_client_mod)
                    else:
                        self.cam_blocked.remove(user.nick)
                        self.send_bot_msg('*' + user.nick + '* is no longer cam blocked.', self.is_client_mod)
                else:
                    self.send_bot_msg('User not found.', self.is_client_mod)

    def do_mute(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            self.send_mute_msg()
            self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + " muted the room")

    def do_instant_push2talk(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            self.send_push2talk_msg()

    def do_auto_pm(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if len(CONFIG['pm_msg']) is not 0:
                CONFIG['auto_pm'] = not CONFIG['auto_pm']
                self.send_bot_msg('*Auto PM*: *' + str(CONFIG['auto_pm']) + '*', self.is_client_mod)
            else:
                self.send_bot_msg('There is no PM message set in the configuraton.',
                                  self.is_client_mod)


    def do_private_room(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            self.send_private_room_msg()
            self.private_room = not self.private_room
            self.send_bot_msg('Private Room is now set to: *' + str(self.private_room) + '*', self.is_client_mod)

    def do_botter(self, new_botter):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod:
            if len(new_botter) is 0:
                self.send_bot_msg('M8 I need a user to block.')
            else:
                if  new_botter not in self.botters:
                    self.botters.append(new_botter)
                    self.send_bot_msg('*' + new_botter + '*' +' was added as to the block list temporarly', self.is_client_mod)
                    return
                else:
                    self.botters.remove(new_botter)
                    self.send_bot_msg('*' + new_botter + '* was removed from block list.', self.is_client_mod)
                    return
        else:
            pass

    def do_autoforgive(self, new_autoforgive):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod:
            if len(new_autoforgive) is not 0:
                autoforgive_user = self.find_user_info(new_autoforgive)
                if autoforgive_user is not None:
                    if autoforgive_user.user_account and autoforgive_user.user_account not in self.autoforgive:
                        self.autoforgive.append(autoforgive_user.user_account)
                        self.send_bot_msg(new_autoforgive + ' is now protected.', self.is_client_mod)
                        pinylib.fh.file_writer(CONFIG['path'], CONFIG['autoforgive'], autoforgive_user.user_account)
                    elif not autoforgive_user.user_account:
                        self.send_bot_msg('Protection is only available to users with accounts.',
                            self.is_client_mod)
                    else:
                        for x in range(len(self.autoforgive)):
                            if self.autoforgive[x] == autoforgive_user.user_account:
                                del self.autoforgive[x]
                                pinylib.fh.remove_from_file(CONFIG['path'], CONFIG['autoforgive'],
                                                            autoforgive_user.user_account)
                                self.send_bot_msg(new_autoforgive + ' is no longer protected.', self.is_client_mod)
                                break
                else:
                    self.send_bot_msg('No user named: ' + new_autoforgive,
                                      self.is_client_mod)
            else:
                self.send_bot_msg('Please state a nickname to protect.',
                                  self.is_client_mod)

    def do_close_broadcast(self, nick_name):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(nick_name) is 0:
                    self.send_bot_msg('Missing nickname.', self.is_client_mod)
                else:
                    user = self.find_user_info(nick_name)
                    if user is not None:
                        self.send_close_user_msg(nick_name)
                        self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + " closed " + user)
                    else:
                        self.send_bot_msg('No nickname called: ' + nick_name, self.is_client_mod)
                        self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + 
                        " attempted to close " + nick_name)

    def do_clear(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                for x in range(0, 25):
                    self.send_owner_run_msg(' ')
            else:
                clear = '133,133,133,133,133,133,133,133,133,133,133,133,133,133,133,133,133,133,133' \
                        '133,133,133,133,133,133,133,133,133,133,133,133,133,133,133,133,133,133,133'
                self._send_command('privmsg', [clear, u'#262626,en'])
                self.console_write(pinylib.COLOR['bright_magenta'], "[status] " + self.user_obj.nick + 
                        " cleared the chat")

    def do_nick(self, new_nick):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if len(new_nick) is 0:
                self.client_nick = pinylib.create_random_string(5, 25)
                self.set_nick()
            else:
                if re.match('^[][\{\}a-zA-Z0-9_-]{1,25}$', new_nick):
                    self.client_nick = new_nick
                    self.set_nick()
                    self.console_write(pinylib.COLOR['bright_yellow'], "[core] " + self.user_obj.nick + " set nickname to " + new_nick)

    def do_topic(self, topic):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(topic) is 0:
                    self.send_topic_msg('')
                    self.send_bot_msg('Topic was *cleared&.', self.is_client_mod)
                    self.console_write(pinylib.COLOR['bright_yellow'], "[core] " + self.user_obj.nick + " cleared the topic")
                else:
                    self.send_topic_msg(topic)
                    self.send_bot_msg('The *room topic* was set to: ' + topic, self.is_client_mod)
                    self.console_write(pinylib.COLOR['bright_yellow'], "[core] " + self.user_obj.nick + " set the topic to: " + topic)
            else:
                self.send_bot_msg('Command not enabled.')

    def do_current_topic(self):
        self.send_undercover_msg(self.user_obj.nick, 'The *current topic* is: %s' % self.topic_msg)
        self.console_write(pinylib.COLOR['bright_yellow'], "[core] " + self.user_obj.nick + " requested the topic")

    def do_kick(self, nick_name, ban=True, discrete=False):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(nick_name) is 0:
                    if not discrete:
                        self.send_bot_msg('Missing nickname.', self.is_client_mod)
                elif nick_name == self.client_nick:
                    if not discrete:
                        self.send_bot_msg('Lol yeah right', self.is_client_mod)
                else:
                    user = self.find_user_info(nick_name)
                    if user is not None:
                        if user.is_owner or user.is_super or user.is_mod:
                            if not discrete:
                                self.send_bot_msg('Banning mods? Don\'t be a dick.', self.is_client_mod)
                            return
                        self.send_ban_msg(user.nick, user.id)
                        if not ban:
                            self.send_forgive_msg(user.id)

                    else:
                        if not discrete:
                            self.send_bot_msg('No user named: *' + nick_name + '*', self.is_client_mod)
            else:
                if not discrete:
                    self.send_bot_msg('Command not enabled.')

    def do_forgive(self, nick_name):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
                if len(nick_name) is not 0:
                    if nick_name == '-all':
                        threading.Thread(target=self.do_forgive_all).start()
                    elif nick_name in self.room_banlist:
                        uid = self.room_banlist[nick_name]
                        self.send_forgive_msg(str(uid))
                        self.send_bot_msg('*' + nick_name + '* has been forgiven', self.is_client_mod)
                    else:
                        self.send_bot_msg('The user was not found in the banlist', self.is_client_mod)
                else:
                    self.send_bot_msg('Please state a nick to forgive from the ban list',
                                      self.is_client_mod)

    def do_forgive_all(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            print(pinylib.send_banlist_msg())
            print(self.room_banlist)
            for x in self.room_banlist:
                self.send_forgive_msg(x)
                print(self.room_banlist)
                #pinylib.time.sleep(0.2)

    def do_bad_nick(self, bad_nick):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(bad_nick) is 0:
                    self.send_bot_msg('Missing nickname.', self.is_client_mod)
                else:
                    user = self.find_user_info(bad_nick)
                    # temp fix
                    if 1 == 0:
                        self.send_bot_msg('*I reckon ' + bad_nick + ' is used by a mod, ' + self.user_obj.nick + '. So no I won\'t add them to the autoban list you fucking piece of shit.*', self.is_client_mod)
                    else:
                        badnicks = pinylib.fh.file_reader(CONFIG['path'], CONFIG['badnicks'])
                        if badnicks is None:
                            pinylib.fh.file_writer(CONFIG['path'], CONFIG['badnicks'], bad_nick)
                        else:
                            if bad_nick in badnicks:
                                self.send_bot_msg(bad_nick + ' is already in the list.', self.is_client_mod)
                            else:
                                pinylib.fh.file_writer(CONFIG['path'], CONFIG['badnicks'], bad_nick)
                                self.send_bot_msg('*' + bad_nick + '* was added to the bad nicks list', self.is_client_mod)
                                if bad_nick in self.room_users.keys():
                                    bn_user = self.find_user_info(bad_nick)
                                    self.send_ban_msg(bn_user.nick, bn_user.id)

    def do_remove_bad_nick(self, bad_nick):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(bad_nick) is 0:
                    self.send_bot_msg('Missing nickname', self.is_client_mod)
                else:
                    rem = pinylib.fh.remove_from_file(CONFIG['path'], CONFIG['badnicks'], bad_nick)
                    if rem:
                        self.send_bot_msg(bad_nick + ' was removed from bad strings', self.is_client_mod)

    def do_bad_string(self, bad_string):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(bad_string) is 0:
                    self.send_bot_msg('Bad string can\'t be blank.', self.is_client_mod)
                elif len(bad_string) < 3:
                    self.send_bot_msg('Bad string to short: ' + str(len(bad_string)), self.is_client_mod)
                else:
                    bad_strings = pinylib.fh.file_reader(CONFIG['path'], CONFIG['badstrings'])
                    if bad_strings is None:
                        pinylib.fh.file_writer(CONFIG['path'], CONFIG['badstrings'], bad_string)
                    else:
                        if bad_string in bad_strings:
                            self.send_bot_msg(bad_string + ' is already in list', self.is_client_mod)
                        else:
                            pinylib.fh.file_writer(CONFIG['path'], CONFIG['badstrings'], bad_string)
                            self.send_bot_msg('*' + bad_string + '* was added to the bad strings list', self.is_client_mod)

    def do_remove_bad_string(self, bad_string):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(bad_string) is 0:
                    self.send_bot_msg('Missing word string.', self.is_client_mod)
                else:
                    rem = pinylib.fh.remove_from_file(CONFIG['path'], CONFIG['badstrings'], bad_string)
                    if rem:
                        self.send_bot_msg(bad_string + ' was removed.', self.is_client_mod)

    def do_bad_account(self, bad_account_name):
        if self.user_obj.is_owner or self.user_obj.is_mod:
            if self.is_client_mod:
                if len(bad_account_name) is 0:
                    self.send_bot_msg('Account can\'t be blank.', self.is_client_mod)
                elif len(bad_account_name) < 3:
                    self.send_bot_msg('Account to short: ' + str(len(bad_account_name)), self.is_client_mod)
                else:
                    user = self.find_user_info(bad_account_name)
                    # Temp fix
                    if 1 == 0:
                        self.send_bot_msg('*' + bad_account_name + ' appears to be a mod m8.*', self.is_client_mod)
                    else:
                        bad_accounts = pinylib.fh.file_reader(CONFIG['path'], CONFIG['badaccounts'])
                        if bad_accounts is None:
                            pinylib.fh.file_writer(CONFIG['path'], CONFIG['badaccounts'], bad_account_name)
                        else:
                            if bad_account_name in bad_accounts:
                                self.send_bot_msg(bad_account_name + ' is already in list.', self.is_client_mod)
                            else:
                                pinylib.fh.file_writer(CONFIG['path'], CONFIG['badaccounts'], bad_account_name)
                                self.send_bot_msg('*' + bad_account_name + '* was added to file.', self.is_client_mod)
                                for key in self.room_users.keys():
                                    user = self.find_user_info(key)
                                    if user.user_account == bad_account_name:
                                        self.send_ban_msg(user.nick, user.id)
                                        break

    def do_remove_bad_account(self, bad_account):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(bad_account) is 0:
                    self.send_bot_msg('Missing account.', self.is_client_mod)
                else:
                    rem = pinylib.fh.remove_from_file(CONFIG['path'], CONFIG['badaccounts'], bad_account)
                    if rem:
                        self.send_bot_msg(bad_account + ' was removed.', self.is_client_mod)

    def do_user_info(self, nick_name):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(nick_name) is 0:
                    self.send_bot_msg('Missing nickname.', self.is_client_mod)
                else:
                    user = self.find_user_info(nick_name)
                    if user is None:
                        self.send_bot_msg('No user named: ' + nick_name,
                                          self.is_client_mod)
                    else:
                        self.send_owner_run_msg('*ID:* %s' % user.id)
                        self.send_owner_run_msg('*Owner:* %s' % user.is_owner)
                        self.send_owner_run_msg('*Is Mod:* %s' % user.is_mod)
                        self.send_owner_run_msg('*Device Type:* %s' % user.device_type)
                        if not user.is_owner and not user.is_super and not user.is_mod:
                            if user.nick or user.user_account in self.botters or self.botteraccounts:
                                self.send_owner_run_msg('*Bot Privileges:* %s' % user.has_power)
                        # TODO: It doesn't print user account type or account gift points.
                        if user.tinychat_id is not None:
                            self.send_undercover_msg(self.user_obj.nick, '*User Account Type:* %s'
                                                     % str(user.user_account_type))
                            self.send_undercover_msg(self.user_obj.nick, '*User Account Gift Points:* %s'
                                                     % str(user.user_account_giftpoints))
                            self.send_undercover_msg(self.user_obj.nick, '*Account:* $s' % str(user.user_account))
                            self.send_undercover_msg(self.user_obj.nick, '*Tinychat ID:* %s' % str(user.tinychat_id))
                            self.send_undercover_msg(self.user_obj.nick, '*Last login:* %s' % str(user.last_login))
                        self.send_owner_run_msg('*Last message:* %s' % str(user.last_msg))

    def do_youtube_search(self, search_str):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(search_str) is 0:
                    self.send_bot_msg('Missing search term.', self.is_client_mod)
                else:
                    self.search_list = youtube.youtube_search_list(search_str, results=5)
                    if len(self.search_list) is not 0:
                        for i in range(0, len(self.search_list)):
                            v_time = self.to_human_time(self.search_list[i]['video_time'])
                            v_title = self.search_list[i]['video_title']
                            self.send_owner_run_msg('(%s) *%s* %s' % (i, v_title, v_time))
                    else:
                        self.send_bot_msg('Could not find: ' + search_str,
                                          self.is_client_mod)

    def do_play_youtube_search(self, int_choice):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(self.search_list) > 0:
                    try:
                        index_choice = int(int_choice)
                        if 0 <= index_choice <= 4:
                            if self.media_timer_thread is not None and self.media_timer_thread.is_alive()\
                                    and self.playlist_mode:
                                self.playlist.append(self.search_list[index_choice])
                                v_time = self.to_human_time(self.search_list[index_choice]['video_time'])
                                v_title = self.search_list[index_choice]['video_title']
                                self.send_bot_msg('*(' + str(len(self.playlist) - 1) + ') Added:* ' +
                                                  v_title + ' *to playlist.* ' + v_time)
                            else:
                                self.last_played_media = self.search_list[index_choice]
                                self.send_media_broadcast_start(self.search_list[index_choice]['type'],
                                                                self.search_list[index_choice]['video_id'])
                                self.media_event_timer(self.search_list[index_choice]['video_time'])
                        else:
                            self.send_bot_msg('Please make a choice between 0-4',
                                              self.is_client_mod)
                    except ValueError:
                        self.send_bot_msg('Only numbers allowed.', self.is_client_mod)

    def do_clear_playlist(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if len(self.playlist) is not 0:
                pl_length = str(len(self.playlist))
                self.playlist[:] = []
                self.inowplay = 0
                self.send_bot_msg('Deleted ' + pl_length + ' items in the playlist',
                                  self.is_client_mod)
            else:
                self.send_bot_msg('The playlist is empty',
                                  self.is_client_mod)

    def do_media_replay(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.media_timer_thread is not None:
                self.cancel_media_event_timer()
            self.send_media_broadcast_start(self.last_played_media['type'], self.last_played_media['video_id'])
            self.media_event_timer(self.last_played_media['video_time'])

    def do_skip(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if len(self.playlist) is not 0:
                if self.inowplay >= len(self.playlist):
                    self.send_bot_msg('This is the last tune in the playlist.',
                                      self.is_client_mod)
                else:
                    self.cancel_media_event_timer()
                    self.last_played_media = self.playlist[self.inowplay]
                    self.send_media_broadcast_start(self.playlist[self.inowplay]['type'],
                                                    self.playlist[self.inowplay]['video_id'])
                    self.media_event_timer(self.playlist[self.inowplay]['video_time'])
                    self.inowplay += 1  # Prepare the next tune in the playlist.
            else:
                self.send_bot_msg('No tunes to skip. The playlist is empty.',
                                  self.is_client_mod)

    def do_close_media(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.media_timer_thread is not None and self.media_timer_thread.is_alive():
                self.cancel_media_event_timer()
                self.send_media_broadcast_close(self.last_played_media['type'])
                self.console_write(pinylib.COLOR['bright_magenta'], 'Closed the ' + self.last_played_media['type'])

    # == Public Command Methods. ==
    def do_version(self):
        pass    
    def do_help(self):
        # move this to config?
        self.send_bot_msg('*Help Page:* http://programmerfag.com/coco/', self.is_client_mod)

    def do_uptime(self):
        self.send_bot_msg('Uptime: ' + self.to_human_time(self.get_uptime()), self.is_client_mod)

    def do_pmme(self):
        self.send_private_msg('How can I help you ' + self.user_obj.nick +'?', self.user_obj.nick)
    
    # TODO put this in a text file or the youtube db
    def do_stfu(self):
        vid = 'https://www.youtube.com/watch?v=MQLd3ELuqtQ'
        threading.Thread(target=self.do_play_media, args=(self.yt_type, vid, )).start()

    def do_oz(self):
        vid = 'https://youtube.com/watch?=vQQTlDvI3-o'
        threading.Thread(target=self.do_play_media, args=(self.yt_type, vid, )).start()


    #  == Media Related Command Methods. ==
    def do_playlist_status(self):
        if self.is_client_mod:
            if len(self.playlist) is 0:
                self.send_bot_msg('The playlist is empty', self.is_client_mod)
            else:
                inquee = len(self.playlist) - self.inowplay
                self.send_bot_msg(str(len(self.playlist)) + ' item(s) in the playlist. ' + str(inquee) +
                                  ' *Still in queue.*', self.is_client_mod)
        else:
            self.send_bot_msg('Not enabled right now.')

    def do_now_playing(self):
        if self.is_client_mod:
            if self.media_timer_thread is not None and self.media_timer_thread.is_alive():
                self.send_undercover_msg(self.user_obj.nick, self.last_played_media['video_title'] + ' ' +
                                        self.to_human_time(self.last_played_media['video_time']) +
                                        ' Link: https://youtu.be/' + self.last_played_media['video_id'])
            else:
                self.send_undercover_msg(self.user_obj.nick, 'No track is playing.')

    def do_next_tune_in_playlist(self):
        if self.is_client_mod:
            if len(self.playlist) is 0:
                self.send_bot_msg('No tunes in the playlist.', self.is_client_mod)
            elif self.inowplay < len(self.playlist):
                play_time = self.to_human_time(self.playlist[self.inowplay]['video_time'])
                play_title = self.playlist[self.inowplay]['video_title']
                self.send_bot_msg('(' + str(self.inowplay) + ') ' + play_title + ' ' +
                                  play_time, self.is_client_mod)
            elif self.inowplay >= len(self.playlist):
                self.send_bot_msg('This is the last tune in the playlist.',
                                  self.is_client_mod)
        else:
            self.send_bot_msg('Not enabled right now.')

    # TODO: Implement logging here, needs to be set to work correctly.
    def do_add_media_to_playlist(self, media_type, search_str):
        log.info('User: %s:%s is searching %s: %s' % (self.user_obj.nick, self.user_obj.id, media_type, search_str))
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                type_str = ''
                if media_type == self.yt_type:
                    type_str = 'YouTube'
                elif media_type == self.sc_type:
                    type_str = 'SoundCloud'

                if len(search_str) is 0:
                    self.send_bot_msg(
                        'Please specify *' + type_str + '* title, id or link.',
                        self.is_client_mod)
                else:
                    _media = None
                    if media_type == self.yt_type:
                        _media = youtube.youtube_search(search_str)
                    elif media_type == self.sc_type:
                        _media = soundcloud.soundcloud_search(search_str)

                    if _media is None:
                        log.warning('%s request returned: %s' % (media_type, _media))
                        self.send_bot_msg('Could not find media: ' + search_str,
                                          self.is_client_mod)
                    else:
                        log.info('%s found: %s' % (media_type, _media))
                        if self.media_timer_thread is not None and self.media_timer_thread.is_alive()\
                                and self.playlist_mode:
                            self.playlist.append(_media)
                            self.send_bot_msg('Added: ' + _media['video_title'] + self.to_human_time(_media['video_time']),
                                              self.is_client_mod)
                        else:
                            self.last_played_media = _media
                            self.send_media_broadcast_start(_media['type'], _media['video_id'])
                            self.media_event_timer(_media['video_time'])
                            self.inowplay += 1  # Prepare the next tune in the playlist.
            else:
                self.send_bot_msg('Not enabled right now.')

    def do_play_media(self, media_type, search_str):
        if self.is_client_mod:
            if self.user_obj.nick in self.botters:
                self.send_bot_msg('lol no.')
                pass
            else:
                if self.lltc_mode is True:
                    self.send_bot_msg('*LLTC mode enabled :D*', self.is_client_mod)
                else:
            # if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
                    type_str = ''
                    if media_type == self.yt_type:
                        type_str = 'YouTube'
                    elif media_type == self.sc_type:
                        type_str = 'SoundCloud'

                    if search_str == "-last":
                        print(self.last_yt)
                        search_str = self.last_yt

                    if len(search_str) is 0:
                        self.send_bot_msg(
                            'Please specify *' + type_str + ' title, id or link.*',
                            self.is_client_mod)
                    else:
                        # Search for the specified media.
                        _media = None
                        if media_type == self.yt_type:
                            _media = youtube.youtube_search(search_str)
                        elif media_type == self.sc_type:
                            _media = soundcloud.soundcloud_search(search_str)

                        # Handle starting media playback.
                        if _media is None:
                            self.send_bot_msg('Could not find media: ' + search_str,
                                              self.is_client_mod)
                        else:
                            if self.media_timer_thread is not None and self.media_timer_thread.is_alive() \
                                    and self.playlist_mode:
                                self.playlist.append(_media)
                                self.send_bot_msg('*' + _media['video_title'] + ' at #' + str(len(self.playlist) - 1) + '*', self.is_client_mod)


                            else:
                                self.last_played_media = _media
                                self.send_media_broadcast_start(_media['type'], _media['video_id'])
                                self.media_event_timer(_media['video_time'])
        else:
            self.send_bot_msg('Not enabled right now.')

    # TODO: Private media handling(?)
    def do_play_private_media(self, media_type, search_str):
        if self.is_client_mod:
            type_str = ''
            if media_type == self.yt_type:
                type_str = 'YouTube'
            elif media_type == self.sc_type:
                type_str = 'SoundCloud'

            if len(search_str) is 0:
                self.send_undercover_msg(self.user_obj.nick, 'Please specify a ' + type_str + ' title, id or link')
            else:
                _media = None
                if media_type == self.yt_type:
                    _media = youtube.youtube_search(search_str)
                elif media_type == self.sc_type:
                    _media = soundcloud.soundcloud_search(search_str)

                if _media is None:
                    self.send_undercover_msg(self.user_obj.nick, 'Could not find video: ' + search_str)
                else:
                    self.user_obj.private_media = media_type
                    self.send_media_broadcast_start(media_type, _media['video_id'], private_nick=self.user_obj.nick)
        else:
            self.send_bot_msg('Not enabled right now.')

    def do_stop_private_media(self):
        self.send_media_broadcast_close(self.yt_type, self.user_obj.nick)
        self.send_media_broadcast_close(self.sc_type, self.user_obj.nick)
    
    def do_sync_media(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if not self.syncing:
                if self.media_timer_thread is not None and self.media_timer_thread.is_alive():
                    self.syncing = True
                    for user in self.room_users.keys():
                        if user != self.client_nick:
                            self.send_media_broadcast_start(self.last_played_media['type'],
                                                            self.last_played_media['video_id'],
                                                            self.current_media_time_point(), private_nick=user)
                    self.syncing = False
                else:
                    self.send_bot_msg('No media is playing to *sync* at the moment.', self.is_client_mod)
            else:
                self.send_bot_msg('A room sync request is currently being processed.', self.is_client_mod)

    def do_sync_media_user(self):
        """ Syncs the media that is currently being playing to the user who requested it. """
        if self.media_timer_thread is not None and self.media_timer_thread.is_alive():
            # Send the media at the correct start time from the playlist to the user.
            self.send_media_broadcast_start(self.last_played_media['type'], self.last_played_media['video_id'],
                    self.current_media_time_point(), private_nick=self.user_obj.nick)
        else:
            self.send_undercover_msg(self.user_obj.nick, 'Nothing playing in the playlist to *sync* at the moment.')
    
    def do_youtube_playlist_videos(self, playlist):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if len(playlist) is 0:
                self.send_bot_msg('Please enter a playlist url or playlist ID.', self.is_client_mod)
            else:
                playlist_id = ''
                if '=' in str(playlist):
                    location_equal = str(playlist).index('=')
                    playlist_id = playlist[location_equal + 1:len(playlist)]
                else:
                    if 'http' or 'www' or 'youtube' not in playlist:
                        playlist_id = str(playlist)

                self.send_bot_msg('Fetching playlist data', self.is_client_mod)

                video_list, non_public = youtube.youtube_playlist_videos(playlist_id)
                if len(video_list) is 0:
                    self.send_bot_msg('No videos in playlist or none were found.', self.is_client_mod)
                else:
                    if non_public > 0:
                        playlist_message = 'Added *' + str(len(video_list)) + ' videos* to the playlist. ' +\
                                           'There were *' + str(non_public) + '* non-public videos.'
                    else:
                        playlist_message = 'Added *' + str(len(video_list)) + ' videos* to the playlist.'

                    if self.media_timer_thread is not None and self.media_timer_thread.is_alive():
                        self.playlist.extend(video_list)
                        self.send_bot_msg(playlist_message, self.is_client_mod)
                    else:
                        self.playlist.extend(video_list)
                        self.last_played_media = self.playlist[self.inowplay]
                        self.send_media_broadcast_start(self.playlist[self.inowplay]['type'],
                                                        self.playlist[self.inowplay]['video_id'])
                        self.media_event_timer(self.playlist[self.inowplay]['video_time'])
                        self.inowplay += 1  # Prepare the next tune in the playlist.
                        self.send_bot_msg(playlist_message, self.is_client_mod)

    def do_youtube_playlist_search(self, playlist_search):
        log.info('User %s:%s is searching a YouTube playlist: %s' % (self.user_obj.nick, self.user_obj.id,
                                                                     playlist_search))
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if len(playlist_search) is 0:
                self.send_bot_msg('Please enter a playlist search query.', self.is_client_mod)
            else:
                self.search_play_lists = youtube.youtube_playlist_search(playlist_search, results=4)
                if self.search_play_lists is None:
                    log.warning('The search returned an error.')
                    self.send_bot_msg('There was an error while fetching the results.', self.is_client_mod)
                elif len(self.search_play_lists) is 0:
                    self.send_bot_msg('The search returned no results.', self.is_client_mod)
                else:
                    log.info('YouTube playlist were found: %s' % self.search_play_lists)
                    for x in range(len(self.search_play_lists)):
                        self.send_undercover_msg(self.user_obj.nick, '*' + str(x + 1) + '. ' +
                                                 self.search_play_lists[x]['playlist_title'] + ' - ' +
                                                 self.search_play_lists[x]['playlist_id'] + '*')
                        pinylib.time.sleep(0.2)

    def do_youtube_playlist_search_choice(self, index_choice):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if len(self.search_play_lists) is 0:
                self.send_bot_msg('No previous playlist search committed to confirm ID. Please do *!plsh*.',
                                  self.is_client_mod)
            elif len(index_choice) is 0:
                self.send_bot_msg('Please choose your selection from the playlist IDs,  e.g. *!pladd 2*',
                                  self.is_client_mod)
            else:
                if 0 <= int(index_choice) <= 4:
                    threading.Thread(target=self.do_youtube_playlist_videos,
                                     args=(self.search_play_lists[int(index_choice) - 1]['playlist_id'],)).start()

    def do_charts(self):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            self.send_bot_msg('Fetching Top40 list...', self.is_client_mod)

            songs_list = other_apis.top40()
            top40_list = list(reversed(songs_list))
            if songs_list is None:
                self.send_bot_msg('We could not fetch the Top40 songs list', self.is_client_mod)
            elif len(songs_list) is 0:
                self.send_bot_msg('No songs were found', self.is_client_mod)
            else:
                video_list = []
                for x in range(len(top40_list)):
                    search_str = top40_list[x][0] + ' - ' + top40_list[x][1]
                    _youtube = youtube.youtube_search(search_str)
                    if _youtube is not None:
                        video_list.append(_youtube)

                if len(video_list) > 0:
                    self.send_bot_msg('Added Top40 songs (40 --> 1) to playlist.',
                                      self.is_client_mod)

                    if self.media_timer_thread is not None and self.media_timer_thread.is_alive():
                        self.playlist.extend(video_list)
                    else:
                        self.playlist.extend(video_list)
                        self.last_played_media = self.playlist[self.inowplay]
                        self.send_media_broadcast_start(self.playlist[self.inowplay]['type'],
                                                        self.playlist[self.inowplay]['video_id'])
                        self.media_event_timer(self.playlist[self.inowplay]['video_time'])
                        self.inowplay += 1  # Prepare the next tune in the playlist.

    def do_lastfm_chart(self, chart_items):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if chart_items is 0 or chart_items is None:
                    self.send_bot_msg('Please specify the amount of tunes you want.',
                                      self.is_client_mod)
                else:
                    try:
                        _items = int(chart_items)
                    except ValueError:
                        self.send_bot_msg('Only numbers allowed', self.is_client_mod)
                    else:
                        if _items > 0:
                            if _items > 30:
                                self.send_bot_msg('No more than 30 tunes',
                                                  self.is_client_mod)
                            else:
                                self.send_bot_msg('Creating playlist...',
                                    self.is_client_mod)
                                last = lastfm.get_lastfm_chart(_items)

                                if last is not None:
                                    if self.media_timer_thread is not None and self.media_timer_thread.is_alive():
                                        self.playlist.extend(last)
                                        self.send_bot_msg('*Added:* ' + str(len(last)) +
                                                          ' *tunes from last.fm chart.*', self.is_client_mod)
                                    else:
                                        self.playlist.extend(last)
                                        self.send_bot_msg('*Added:* ' + str(len(last)) +
                                                          ' *tunes from last.fm chart.*', self.is_client_mod)
                                        self.last_played_media = self.playlist[self.inowplay]
                                        self.send_media_broadcast_start(self.playlist[self.inowplay]['type'],
                                                                        self.playlist[self.inowplay]['video_id'])
                                        self.media_event_timer(self.playlist[self.inowplay]['video_time'])
                                        self.inowplay += 1  # Prepare the next tune in the playlist.
                                else:
                                    self.send_bot_msg('Failed to retrieve a result from last.fm.', self.is_client_mod)
            else:
                self.send_bot_msg('Not enabled right now.')

    def do_lastfm_random_tunes(self, max_tunes):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if max_tunes is 0 or max_tunes is None:
                    self.send_bot_msg('Please specify the max amount of tunes you want.',
                                      self.is_client_mod)
                else:
                    try:
                        _items = int(max_tunes)
                    except ValueError:
                        self.send_bot_msg('Only numbers allowed.', self.is_client_mod)
                    else:
                        if _items > 0:
                            if _items > 25:
                                self.send_bot_msg('No more than 25 tunes.',
                                                  self.is_client_mod)
                            else:
                                self.send_bot_msg('Creating playlist...',
                                    self.is_client_mod)
                                last = lastfm.lastfm_listening_now(max_tunes)

                                if last is not None:
                                    if self.media_timer_thread is not None and self.media_timer_thread.is_alive():
                                        self.playlist.extend(last)
                                        self.send_bot_msg('Added *' + str(
                                            len(last)) + '* tunes from *last.fm*',
                                                          self.is_client_mod)
                                    else:
                                        self.playlist.extend(last)
                                        self.send_bot_msg('Added *' + str(len(last)) +
                                                          ' * tunes from *last.fm*', self.is_client_mod)
                                        self.last_played_media = self.playlist[self.inowplay]
                                        self.send_media_broadcast_start(self.playlist[self.inowplay]['type'],
                                                                        self.playlist[self.inowplay]['video_id'])
                                        self.media_event_timer(self.playlist[self.inowplay]['video_time'])
                                        self.inowplay += 1  # Prepare the next tune in the playlist.
                                else:
                                    self.send_bot_msg(
                                        'Failed to retrieve a result from last.fm.',
                                        self.is_client_mod)
            else:
                self.send_bot_msg('Not enabled right now.')

    def search_lastfm_by_tag(self, search_str):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if self.is_client_mod:
                if len(search_str) is 0:
                    self.send_bot_msg('Missing search tag.', self.is_client_mod)
                else:
                    self.send_bot_msg('Creating playlist...',
                                      self.is_client_mod)
                    last = lastfm.search_lastfm_by_tag(search_str)

                    if last is not None:
                        if self.media_timer_thread is not None and self.media_timer_thread.is_alive():
                            self.playlist.extend(last)
                            self.send_bot_msg('Added *' + str(len(last)) + '* tunes from *last.fm*',
                                self.is_client_mod)
                        else:
                            self.playlist.extend(last)
                            self.send_bot_msg('Added *' + str(len(last)) + '* tunes from *last.fm*',
                                self.is_client_mod)
                            self.last_played_media = self.playlist[self.inowplay]
                            self.send_media_broadcast_start(self.playlist[self.inowplay]['type'],
                                                            self.playlist[self.inowplay]['video_id'])
                            self.media_event_timer(self.playlist[self.inowplay]['video_time'])
                            self.inowplay += 1  # Prepare the next tune in the playlist.
                    else:
                        self.send_bot_msg('Failed to retrieve a result from last.fm.',
                                          self.is_client_mod)
            else:
                self.send_bot_msg('Not enabled right now.')

    def do_delete_playlist_item(self, to_delete):
        usage = '*' + CONFIG['prefix'] + 'del 1* or *' + CONFIG['prefix'] + 'del 1,2,4* or *' \
                + CONFIG['prefix'] + 'del 2:8*'
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if len(self.playlist) is 0:
                self.send_undercover_msg(self.user_obj.nick, 'The playlist is empty.')
            if len(to_delete) is 0:
                self.send_undercover_msg(self.user_obj.nick, usage)
            else:
                indexes = None
                deleted_by_range = False
                playlist_copy = list(self.playlist)
                # using : as a separator.
                if ':' in to_delete:
                    try:
                        range_indexes = map(int, to_delete.split(':'))
                        temp_indexes = range(range_indexes[0], range_indexes[1])
                    except ValueError:
                        self.send_undercover_msg(self.user_obj.nick, usage)
                    else:
                        indexes = []
                        for i in temp_indexes:
                            if i < len(self.playlist):
                                if i not in indexes:
                                    indexes.append(i)
                        if len(indexes) > 1:
                            deleted_by_range = True
                else:
                    try:
                        temp_indexes = map(int, to_delete.split(','))
                    except ValueError:
                        self.send_undercover_msg(self.user_obj.nick, usage)
                    else:
                        indexes = []
                        for i in temp_indexes:
                            if i < len(self.playlist):
                                if i not in indexes:
                                    indexes.append(i)
                deleted_indexes = []
                if indexes is not None and len(indexes) is not 0:
                    if len(self.playlist) is not 0:
                        for i in sorted(indexes, reverse=True):
                            if self.inowplay <= i < len(self.playlist):
                                del self.playlist[i]
                                deleted_indexes.append(str(i))
                        deleted_indexes.reverse()
                        if len(deleted_indexes) > 0:
                            if deleted_by_range:
                                self.send_bot_msg('*deleted: index range from (and including)* ' +
                                                  str(deleted_indexes[0]) + ' to ' + str(deleted_indexes[-1]),
                                                  self.is_client_mod)
                            elif len(deleted_indexes) is 1:
                                self.send_bot_msg('Deleted: *' + playlist_copy[int(deleted_indexes[0])]['video_title'] +
                                                  '*', self.is_client_mod)
                            else:
                                self.send_bot_msg('*Deleted tracks at index:* ' + ', '.join(deleted_indexes),
                                                  self.is_client_mod)
                        else:
                            self.send_bot_msg('Nothing was deleted.', self.is_client_mod)
                    else:
                        self.send_bot_msg('The playlist is empty, no tracks to delete.', self.is_client_mod)

    # == Tinychat API Command Methods. ==
    def do_spy(self, room_name):
        if self.is_client_mod:
            if len(room_name) is 0:
                self.send_undercover_msg(self.user_obj.nick, 'Missing room name.')
            else:
                spy_info = pinylib.tinychat_api.spy_info(room_name)
                if spy_info is None:
                    self.send_undercover_msg(self.user_obj.nick, 'The room is empty.')
                elif spy_info == 'PW':
                    self.send_undercover_msg(self.user_obj.nick, 'The room is password protected.')
                else:
                    self.send_undercover_msg(self.user_obj.nick,
                                             '*mods:* ' + spy_info['mod_count'] +
                                             ' *Broadcasters:* ' + spy_info['broadcaster_count'] +
                                             ' *Users:* ' + spy_info['total_count'])
                    if self.user_obj.is_owner or self.user_obj.is_mod or self.user_obj.has_power:
                        users = ', '.join(spy_info['users'])
                        self.send_undercover_msg(self.user_obj.nick, '*' + users + '*')
        else:
            self.send_bot_msg('Not enabled right now.')

    def do_account_spy(self, account):
        if self.is_client_mod:
            if len(account) is 0:
                self.send_undercover_msg(self.user_obj.nick, 'Missing username to search for.')
            else:
                tc_usr = pinylib.tinychat_api.tinychat_user_info(account)
                if tc_usr is None:
                    self.send_undercover_msg(self.user_obj.nick, 'Could not find Tinychat info for: %s' % account)
                else:
                    self.send_undercover_msg(self.user_obj.nick, 'ID: %s, Last login: %s, Location: %s' %
                                             (tc_usr['tinychat_id'], tc_usr['last_active'], tc_usr['location']))
        else:
            self.send_bot_msg('Not enabled right now.')

    # == Other API Command Methods. ==
    def do_search_urban_dictionary(self, search_str):
        restricted_terms = ['spam']
        begin = '*Urban Dictionary:* '

        if self.is_client_mod:
            if len(search_str) is 0:
                self.send_bot_msg('Please specify something to look up.',
                                  self.is_client_mod)
            elif search_str in restricted_terms:
                self.send_bot_msg('This is a restricted term.', self.is_client_mod)
            else:
                urban = other_apis.urbandictionary_search(search_str)
                urban = urban.strip()
                if urban is None:
                    self.send_bot_msg('Could not find a definition for: ' + search_str,
                                      self.is_client_mod)
                else:
                    if len(urban) > 70:
                        urb_parts = urban.split('.')
                        self.send_bot_msg(begin + urb_parts[0].strip('\n'), self.is_client_mod)
                        #self.send_bot_msg(urb_parts[1], self.is_client_mod)
                    else:
                        self.send_bot_msg(begin + urban.strip('\n'), self.is_client_mod)
        else:
            self.send_bot_msg('Not enabled right now.')

    def do_weather_search(self, search_str):
        if len(search_str) is 0:
            self.send_bot_msg('Please specify a city to search for.', self.is_client_mod)
        else:
            weather = other_apis.weather_search(search_str)
            if weather is None:
                self.send_bot_msg('Could not find weather data for: ' + search_str,
                                  self.is_client_mod)
            elif not weather:
                self.send_bot_msg('Missing API key.', self.is_client_mod)
            else:
                self.send_bot_msg(weather, self.is_client_mod)

    def do_whois_ip(self, ip_str):
        if len(ip_str) is 0:
            self.send_bot_msg('Please provide an IP address.', self.is_client_mod)
        else:
            whois = other_apis.whois(ip_str)
            if whois is None:
                self.send_bot_msg('No info found for: ' + ip_str, self.is_client_mod)
            else:
                self.send_bot_msg(whois)

    def do_duckduckgo_search(self, search):
        if len(search) is 0:
            self.send_bot_msg('Please enter a *DuckDuckGo* search term.',
                              self.is_client_mod)
        else:
            definitions = other_apis.duckduckgo_search(search)
            if definitions is not None:
                for x in range(len(definitions)):
                    if len(definitions[x]) > 160:
                        sentence = definitions[x][0:159] + '\n ' + definitions[x][159:]
                    else:
                        sentence = definitions[x]
                    self.send_bot_msg(str(x + 1) + ' *' + sentence + '*', self.is_client_mod)

    def do_wiki_search(self, search_str):
        if len(search_str) is 0:
            self.send_bot_msg('Please specify something to look up on *Wikipedia*.',
                              self.is_client_mod)
        else:
            wiki = other_apis.wiki_search(search_str)
            if wiki is None:
                self.send_bot_msg('There was an error with the *Wikipedia* search.',
                                  self.is_client_mod)
            elif wiki is False:
                self.send_bot_msg('*No Wikipedia module installed!* -- "pip install wikipedia"', self.is_client_mod)
            else:
                # This bit probably isn't needed since the wikipedia function should only return two sentences
                if len(wiki) > 70:
                    if '.' in str(wiki):
                        wiki_parts = str(wiki).split('.')
                        self.send_bot_msg(u'' + wiki_parts[0].strip(), self.is_client_mod)
                else:
                    self.send_bot_msg(u'' + wiki.strip(), self.is_client_mod)

    def do_omdb_search(self, search):
        if len(search) is 0:
            self.send_bot_msg('Please specify a movie or television show.', self.is_client_mod)
        else:
            omdb = other_apis.omdb_search(search)
            if omdb is None:
                self.send_bot_msg('Error or title does not exist.', self.is_client_mod)
            else:
                self.send_bot_msg(omdb, self.is_client_mod)

    def do_etymonline_search(self, search):
        if len(search) is 0:
            self.send_bot_msg('Please enter a search term.', self.is_client_mod)
        else:
            etymology = other_apis.etymonline(search)
            if etymology is None:
                self.send_bot_msg('We could not retrieve the etymology for your term.',
                                  self.is_client_mod)
            else:
                self.send_bot_msg(etymology, self.is_client_mod)

    
    def do_dict(self, search):
        if len(search) is 0:
            self.send_bot_msg('Search was empty mate.', self.is_client_mod)
        else:
            diction = other_apis.defdict(search)
            self.send_bot_msg(diction, self.is_client_mod)
    
    def do_chuck_norris(self):
        chuck = other_apis.chuck_norris()
        if chuck is not None:
            self.send_bot_msg(chuck, self.is_client_mod)
        else:
            self.send_bot_msg('Unable to retrieve from server.', self.is_client_mod)

    def do_8ball(self, question):
        if len(question) is 0:
            self.send_bot_msg('Provide a yes/no question.', self.is_client_mod)
        else:
            self.send_bot_msg('*8Ball:* ' + eightball(), self.is_client_mod)

    def do_auto_url(self, msg):
        if 'http' in msg:
            if '!' not in msg and 'tinychat.com' not in msg:
                url = None
                if msg.startswith('http://'):
                    url = msg.split('http://')[1]
                    msgs = url.split(' ')[0]
                    url = auto_url.auto_url('http://' + msgs)
                elif msg.startswith('https://'):
                    url = msg.split('https://')[1]
                    msgs = url.split(' ')[0]
                    url = auto_url.auto_url('https://' + msgs)
                if url is not None:
                    self.send_bot_msg('*[ ' + url + ' ]*', self.is_client_mod)
                    self.console_write(pinylib.COLOR['cyan'], self.user_obj.nick + ' posted a URL: ' + url)

    def do_yo_mama_joke(self):
        yo_mama = str(other_apis.yo_mama_joke())
        if yo_mama is not None:
            self.send_bot_msg('*' + str(self.user_obj.nick) + '* says ' + yo_mama.lower(), self.is_client_mod)
        else:
            self.send_bot_msg('Unable to retrieve from server.', self.is_client_mod)

    def do_advice(self):
        advice = other_apis.online_advice()
        if advice is not None:
            self.send_bot_msg('*' + str(self.user_obj.nick) + '*, ' + advice.lower(), self.is_client_mod)
        else:
            self.send_bot_msg('Unable to retrieve from server.', self.is_client_mod)

    def do_one_liner(self, tag):
        if tag:
            if tag == '?':
                all_tags = ', '.join(other_apis.tags) + '.'
                self.send_undercover_msg(self.user_obj.nick, '*Possible tags*: ' + str(all_tags))
                return
            elif tag in other_apis.tags:
                one_liner = other_apis.one_liners(tag)
            else:
                self.send_bot_msg('The tag you specified is not available. Enter *!joke ?* to get a list of tags.',
                                  self.is_client_mod)
                return
        else:
            one_liner = other_apis.one_liners()

        if one_liner is not None:
            self.send_bot_msg('*' + one_liner + '*', self.is_client_mod)
        else:
            self.send_bot_msg('Unable to retrieve from server.', self.is_client_mod)

    def do_time(self, location):
        if len(location) is 0:
            self.send_bot_msg('Please enter a location to fetch the time.',
                              self.is_client_mod)
        else:
            time_list = other_apis.google_time(location)
            if time_list is None:
                self.send_bot_msg('We could not fetch the time in *"' +
                                  str(location) + '"*.', self.is_client_mod)
            else:
                location = time_list[1].strip()
                self.send_bot_msg(location + ': *' + time_list[0] + '*', self.is_client_mod)

    def do_time_additional(self, location):
        if len(location) is 0:
            self.send_bot_msg('Please enter a location to fetch the time.', self.is_client_mod)
        else:
            time = other_apis.time_is(location)
            if time is None:
                self.send_bot_msg(
                    'We could not fetch the time in "' + str(location) + '".',
                    self.is_client_mod)
            else:
                self.send_bot_msg('The time in *' + str(location) + '* is: *' + str(time) + "*", self.is_client_mod)

    def do_ascii(self, ascii_id):
        if '!' in ascii_id:
            parts = ascii_id.split('!')
            if len(parts) > 1:
                key = parts[1]
                if key in ascii_dict:
                    self.send_bot_msg('*' + ascii_dict[key] + '*', self.is_client_mod)
                    return True
                else:
                    return None
        else:
            return None

    def do_rate(self, target):
        if len(target) is 0:
            self.send_bot_msg('*Critic:* I can\'t rate nothing.', self.is_client_mod)
        else:
            self.send_bot_msg('*desu:* ' + rate(), self.is_client_mod)

    def do_ddos(self, target):
        if len(target) is 0:
            self.send_bot_msg('*BOTNET:* Need a target to exterminate.', self.is_client_mod)
        else:
            gen_ip = str(random.sample(range(1, 255), 4))
            fix_ip = str.replace(gen_ip, ', ', '.')
            self.send_bot_msg('*BOTNET:* Executing "perl -w skid.pl" on *' + target + ' @ ' + fix_ip.strip('[]') + '*',
                              self.is_client_mod)

    
    def do_age(self, target):
        if len(target) is 0:
            self.send_bot_msg('What?')
        else:
            self.send_bot_msg('*'+target+' is probably ' + str(random.randint(0,100)) + '*', self.is_client_mod)
    def do_jew(self, target):
        response = ['Positive', 'Negative']

        if len(target) is 0:
            self.send_bot_msg('*Jew Detector:* \'Null\' is probably a jew.', self.is_client_mod)
        else:
            self.send_bot_msg('*Jew Detector:* Scanning.....', self.is_client_mod)
            time.sleep(2)
            self.send_bot_msg('*Jew Detector:* Scan of *' + target + '* complete. Result: *' +
                              random.choice(response) + '*', self.is_client_mod)

    def do_fake(self, target):
        races = ['asian', 'indian', 'african', 'white', 'nazi', 'north korean', 'chinese', 'korean', 'kpop star',
        'homosexual', 'cis scum', 'tumblr', 'special snowflake']
        if len(target) is 0:
            self.send_bot_msg('*Donald Trump:* Gimme a name before I bomb you too.', self.is_client_mod)
        else:
            self.send_bot_msg('*Donald Trump:* '+ target + ' is a fake ' + random.choice(races), self.is_client_mod)

    def do_drug(self, target):
        drugs = give_drug()

        if len(target) is 0:
            fix1 = drugs.replace('your', self.user_obj.nick + '\'s')
            fix = fix1.replace('you', self.user_obj.nick)
            self.send_bot_msg('*Drug Dealer:* ' + fix, self.is_client_mod)
        else:
            fix1 = drugs.replace('your', target + '\'s')
            fix = fix1.replace('you', target)
            self.send_bot_msg('*Drug Dealer:* ' + fix, self.is_client_mod)

    def do_tip(self, person):
        tokens = str(random.sample(range(1, 1000), 1))
        if len(person) is 0:
            self.send_bot_msg('*Chaturbate:* Who do you want to tip?', self.is_client_mod)
        else:
            self.send_bot_msg('*' + self.user_obj.nick +'* tips *' + person + '* ' + tokens.strip('[]') + ' tokens.', self.is_client_mod)

    def do_trump(self, person):
        if len(person) is 0:
            self.send_bot_msg('*' + self.user_obj.nick + '* receives a small loan of a million dollars', self.is_client_mod)
        else:
            self.send_bot_msg('*' + self.user_obj.nick + '* gives *' + person + '* a small loan of a million dollars', self.is_client_mod)

    def do_savage(self, target):
        number = str(random.sample(range(1, 20000), 1))
        if len(target) is 0:
            self.send_bot_msg('*' + self.user_obj.nick + '\'s savage-ness is over ' + number.strip('[]') + '!!!!*', self.is_client_mod)
        else:
            self.send_bot_msg('*' + target + '\'s savage-ness is over ' + number.strip('[]') + '!!!!*', self.is_client_mod)

    def do_cat(self):
        self.send_bot_msg('*Catgif:* http://thecatapi.com/api/images/get?format=src&type=gif', self.is_client_mod)

    def do_download(self, url):
        if self.user_obj.is_mod and len(url) > 0:
            self.send_undercover_msg(self.user_obj.nick, 'Starting download.')
            self.send_undercover_msg(self.user_obj.nick, 'Download complete: ' + download.dl_youtube(url))
        else:
            url = 'https://youtu.be/' + self.last_played_media['video_id']
            self.send_undercover_msg(self.user_obj.nick, 'Your download of: ' + self.last_played_media['video_title'] + ' is beginning.')
            self.send_bot_msg('Your download is available at '+ download.dl_youtube(url), self.is_client_mod)
	    #self.send_bot_msg("*Not implemented yet.*", self.is_client_mod)

    def private_message_handler(self, msg_sender, private_msg):
        self.check_msg_for_bad_string(private_msg, True)

        if private_msg.startswith(CONFIG['prefix']):
            pm_parts = private_msg.split(' ')
            pm_cmd = pm_parts[0].lower().strip()
            pm_arg = ' '.join(pm_parts[1:]).strip()

            # Super mod commands.
            if pm_cmd == CONFIG['prefix'] + 'rp':
                threading.Thread(target=self.do_set_room_pass, args=(pm_arg, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'bp':
                threading.Thread(target=self.do_set_broadcast_pass, args=(pm_arg, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'yt':
                threading.Thread(target=self.do_play_media, args=(self.yt_type, pm_arg, )).start()
            
            # Owner and super mod commands.
            elif pm_cmd == CONFIG['prefix'] + 'key':
                threading.Thread(target=self.do_key, args=(pm_arg, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'clrbn':
                threading.Thread(target=self.do_clear_bad_nicks).start()

            elif pm_cmd == CONFIG['prefix'] + 'clrbs':
                threading.Thread(target=self.do_clear_bad_strings).start()

            elif pm_cmd == CONFIG['prefix'] + 'clrba':
                threading.Thread(target=self.do_clear_bad_accounts).start()

            elif pm_cmd == CONFIG['prefix'] + 'lltc':
                threading.Thread(target=self.do_lltc, args=(pm_arg, )).start()
            
            # Mod and bot controller commands.
            elif pm_cmd == CONFIG['prefix'] + 'disconnect':
                self.do_pm_disconnect(pm_arg)

            elif pm_cmd == CONFIG['prefix'] + 'op':
                threading.Thread(target=self.do_op_user, args=(pm_parts, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'deop':
                threading.Thread(target=self.do_deop_user, args=(pm_parts, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'up':
                threading.Thread(target=self.do_cam_up).start()

            elif pm_cmd == CONFIG['prefix'] + 'down':
                threading.Thread(target=self.do_cam_down).start()

            elif pm_cmd == CONFIG['prefix'] + 'nick':
                self.do_nick(pm_arg)

            elif pm_cmd == CONFIG['prefix'] + 'ban':
                threading.Thread(target=self.do_kick, args=(pm_arg, True, True, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'nocam':
                threading.Thread(target=self.do_nocam, args=(pm_arg, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'noguest':
                threading.Thread(target=self.do_no_guest, args=(pm_arg, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'notice':
                if self.is_client_mod:
                    threading.Thread(target=self.send_owner_run_msg, args=(pm_arg, )).start()
                else:
                    threading.Thread(target=self.send_private_msg, args=('Not enabled.', msg_sender)).start()

            elif pm_cmd == CONFIG['prefix'] + 'say':
                threading.Thread(target=self.send_chat_msg, args=(pm_arg, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'setpm':
                threading.Thread(target=self.do_set_auto_pm, args=(pm_arg, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'pmall':
                threading.Thread(target=self.do_pm_all, args=(pm_arg, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'close':
                self.do_close_broadcast(pm_arg)
	    # Public commands.
            #elif pm_cmd == CONFIG['prefix'] + 'sudo':
            #    threading.Thread(target=self.do_super_user, args=(pm_arg, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'opme':
                threading.Thread(target=self.do_opme, args=(pm_arg, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'pm':
                threading.Thread(target=self.do_pm_bridge, args=(pm_parts, )).start()

            elif pm_cmd == CONFIG['prefix'] + 'notice':
                 self.send_bot_msg(pm_arg, self.is_client_mod)

        # Print to console.
        self.console_write(pinylib.COLOR['white'], 'Private message from ' + msg_sender + ': ' + str(private_msg)
                           .replace(self.key, '***KEY***')
                           .replace(CONFIG['super_key'], '***SUPER KEY***'))

    # == Super Mod Command Methods. ==
    def do_set_room_pass(self, password):
        if self.is_client_owner:
            if self.user_obj.is_super:
                if not password:
                    self.privacy_settings.set_room_password()
                    self.send_bot_msg('*The room password was removed.*', self.is_client_mod)
                    pinylib.time.sleep(1)
                    self.send_private_msg('The room password was removed.', self.user_obj.nick)
                elif len(password) > 1:
                    self.privacy_settings.set_room_password(password)
                    self.send_private_msg('*The room password is now:* ' + password, self.user_obj.nick)
                    pinylib.time.sleep(1)
                    self.send_bot_msg('*The room is now password protected.*', self.is_client_mod)

    def do_set_broadcast_pass(self, password):
        if self.is_client_owner:
            if self.user_obj.is_super:
                if not password:
                    self.privacy_settings.set_broadcast_password()
                    self.send_bot_msg('*The broadcast password was removed.*', self.is_client_mod)
                    pinylib.time.sleep(1)
                    self.send_private_msg('The broadcast password was removed.', self.user_obj.nick)
                elif len(password) > 1:
                    self.privacy_settings.set_broadcast_password(password)
                    self.send_private_msg('*The broadcast password is now:* ' + password, self.user_obj.nick)
                    pinylib.time.sleep(1)
                    self.send_bot_msg('*Broadcast password is enabled.*', self.is_client_mod)

    # == Owner And Super Mod Command Methods. ==
    def do_key(self, new_key):
        if self.user_obj.is_owner or self.user_obj.is_super:
            if len(new_key) is 0:
                self.send_private_msg('The current key is: *' + self.key + '*', self.user_obj.nick)
            elif len(new_key) < 6:
                self.send_private_msg('Key must be at least 6 characters long: ' + str(len(self.key)),
                                          self.user_obj.nick)
            elif len(new_key) >= 6:
                self.key = new_key
                self.send_private_msg('The key was changed to: *' + self.key + '*', self.user_obj.nick)

    def do_clear_bad_nicks(self):
        if self.user_obj.is_owner or self.user_obj.is_super:
            pinylib.fh.delete_file_content(CONFIG['path'], CONFIG['badnicks'])

    def do_clear_bad_strings(self):
        if self.user_obj.is_owner or self.user_obj.is_super:
            pinylib.fh.delete_file_content(CONFIG['path'], CONFIG['badstrings'])

    def do_clear_bad_accounts(self):
        if self.user_obj.is_owner or self.user_obj.is_super:
            pinylib.fh.delete_file_content(CONFIG['path'], CONFIG['badaccounts'])

    # == Mod And Bot Controller Command Methods. ==
    def do_pm_disconnect(self, key):
        if self.user_obj.is_owner or self.user_obj.is_super or self.user_obj.is_mod or self.user_obj.has_power:
            if len(key) is 0:
                self.send_private_msg('Missing key.', self.user_obj.nick)
            else:
                if key == self.key:
                    log.info('User %s:%s commenced remote disconnect.' % (self.user_obj.nick, self.user_obj.id))
                    self.send_private_msg('The bot will disconnect from the room.', self.user_obj.nick)
                    self.console_write(pinylib.COLOR['red'], 'Disconnected by %s.' % self.user_obj.nick)
                    threading.Thread(target=self.disconnect).start()
                    # Exit with a normal status code.
                    sys.exit(1)
                else:
                    self.send_private_msg('Wrong key.', self.user_obj.nick)

    def do_op(self, username):
        if self.user_obj.is_owner or self.user_obj.is_mod:
            if len(username) == 0:
                self.send_bot_msg('Missing username.', self.is_client_mod)
            else:
                user = self.find_user_info(username)
                if user is not None:
                    if user.has_power or user.is_mod or user.is_owner:
                        self.send_bot_msg('*' + user.nick + ' already has DUH POWAH!*', self.is_client_mod)
                    else:
                        user.has_power = True
                        self.send_bot_msg('*I reckon we can let ' + user.nick + ' have a go*', self.is_client_mod)
                else:
                    self.send_bot_msg('*No user named ' + username + ' m8.*', self.is_client_mod)

    def do_deop(self, username):
        if self.user_obj.is_owner or self.user_obj.is_mod:
            if len(username) == 0:
                self.send_bot_msg('Missing username.', self.is_client_mod)
            else:
                user = self.find_user_info(username)
                if user is not None:
                    if not user.has_power:
                        self.send_bot_msg('*' + user.nick + ' doesn\'t even have DUH POWAH*', self.is_client_mod)
                    else:
                        user.has_power = False
                        self.send_bot_msg('*' + user.nick + ' can fuck off.*', self.is_client_mod)
                else:
                    self.send_bot_msg('*' + username + ' doesn\'t exist or ' + self.user_obj.nick + ' can\'t spell.*', self.is_client_mod)

    def do_cam_up(self):
        if self.user_obj.is_owner or self.user_obj.is_mod:
            self.send_bauth_msg()
            self._set_stream()
        else:
            pass
        
    def do_cam_down(self):
        if self.user_obj.is_owner or self.user_obj.is_mod:
            self._set_stream(False)
        else:
            pass
        
    def do_nocam(self, key):
        if self.no_cam:
            if self.user_obj.is_owner or self.user_obj.is_super:
                self.no_cam = False
                self.send_private_msg('*Broadcasting is allowed.*', self.user_obj.nick)
            elif self.user_obj.is_mod or self.user_obj.has_power:
                if len(key) is 0:
                    self.send_private_msg('Missing key.', self.user_obj.nick)
                elif key == self.key:
                    self.no_cam = False
                    self.send_private_msg('*Broadcasting is allowed.*', self.user_obj.nick)
                else:
                    self.send_private_msg('Wrong key.', self.user_obj.nick)
        else:
            if self.user_obj.is_owner or self.user_obj.is_super:
                self.no_cam = True
                self.send_private_msg('*Broadcasting is NOT allowed.*', self.user_obj.nick)
            elif self.user_obj.is_mod or self.user_obj.has_power:
                if len(key) is 0:
                    self.send_private_msg('Missing key.', self.user_obj.nick)
                elif key == self.key:
                    self.no_cam = True
                    self.send_private_msg('*Broadcasting is NOT allowed.*', self.user_obj.nick)
                else:
                    self.send_private_msg('Wrong key.', self.user_obj.nick)

    # TODO: no_normal users mode & auto kick all users who do match either criteria.
    #       we may not need this function as we can most likely implement this as a direct command.
    def do_no_guest(self, key):
        if self.no_guests:
            if self.user_obj.is_owner:
                self.no_guests = False
                self.send_private_msg('*Guests ARE allowed to join the room.*', self.user_obj.nick)
            elif self.user_obj.is_mod or self.user_obj.has_power:
                if len(key) is 0:
                    self.send_private_msg('Missing key.', self.user_obj.nick)
                elif key == self.key:
                    self.no_guests = False
                    self.send_private_msg('*Guests ARE allowed to join the room.*', self.user_obj.nick)
                else:
                    self.send_private_msg('Wrong key.', self.user_obj.nick)
        else:
            if self.user_obj.is_owner or self.user_obj.is_super:
                self.no_guests = True
                self.send_private_msg('*Guests are NOT allowed to join the room.*', self.user_obj.nick)
            elif self.user_obj.is_mod or self.user_obj.has_power:
                if len(key) is 0:
                    self.send_private_msg('Missing key.', self.user_obj.nick)
                elif key == self.key:
                    self.no_guests = True
                    self.send_private_msg('*Guests are NOT allowed to join the room.*', self.user_obj.nick)
                else:
                    self.send_private_msg('Wrong key.', self.user_obj.nick)

    def do_set_auto_pm(self, message):
        if self.user_obj.is_mod or self.user_obj.has_power or self.user_obj.is_owner:
            if CONFIG['auto_pm']:
                if len(message) is 0:
                    self.send_private_msg('Please enter a new Room Private Message.', self.user_obj.nick)
                else:
                    CONFIG['pm_msg'] = message
                    self.send_private_msg('Room private message now set to: ' + str(CONFIG['pm_msg']), self.user_obj.nick)
            else:
                self.send_private_msg('Automatic private message feature is not enabled in the configuration.',
                                      self.user_obj.nick)

    def do_pm_all(self, message):
        if self.user_obj.is_mod or self.user_obj.has_power or self.user_obj.is_owner:
            if not self.pmming_all:
                if len(message) is 0:
                    self.send_private_msg('Please enter a message to send to all users.', self.user_obj.nick)
                else:
                    self.pmming_all = True
                    for user in self.room_users.keys():
                        self.send_private_msg(str(message), str(user))
                        pinylib.time.sleep(1.2)
                    self.pmming_all = False
            else:
                self.send_private_msg('There is already a private being sent to all users in the room.', self.is_client_mod)

    # == Public PM Command Methods. ==
    def do_super_user(self, super_key):
        if self.is_client_owner:
            if len(super_key) is 0:
                self.send_private_msg('Missing super key.', self.user_obj.nick)
            elif super_key == CONFIG['super_key']:
                self.user_obj.is_super = True
                self.send_private_msg('*You are now a super mod.*', self.user_obj.nick)
            else:
                self.send_private_msg('Wrong super key.', self.user_obj.nick)
        else:
            self.send_private_msg('Client is owner: *' + str(self.is_client_owner) + '*', self.user_obj.nick)

    def do_opme(self, key):
        if self.user_obj.has_power:
            self.send_private_msg('You already have privileges. No need to OP again.', self.user_obj.nick)
        else:
            if len(key) is 0:
                self.send_private_msg('Missing key.', self.user_obj.nick)
            elif key == self.key:
                self.user_obj.has_power = True
                self.send_private_msg('You are now a bot controller.', self.user_obj.nick)
            else:
                self.send_private_msg('Wrong key.', self.user_obj.nick)

    def do_pm_bridge(self, pm_parts):
        if len(pm_parts) == 1:
            self.send_private_msg('Missing username.', self.user_obj.nick)
        elif len(pm_parts) == 2:
            self.send_private_msg('The command is: ' + CONFIG['prefix'] + 'pm username message', self.user_obj.nick)
        elif len(pm_parts) >= 3:
            pm_to = pm_parts[1]
            msg = ' '.join(pm_parts[2:])
            is_user = self.find_user_info(pm_to)
            if is_user is not None:
                if is_user.id == self.client_id:
                    self.send_private_msg('Action not allowed.', self.user_obj.nick)
                else:
                    self.send_private_msg('*<' + self.user_obj.nick + '>* ' + msg, pm_to)
            else:
                self.send_private_msg('No user named: ' + pm_to, self.user_obj.nick)

    # Media functions.
    def media_event_handler(self):
        if len(self.playlist) is not 0:
            if self.inowplay >= len(self.playlist):
                self.inowplay = 0
                self.playlist[:] = []
            else:
                if self.is_connected:
                    self.last_played_media = self.playlist[self.inowplay]
                    self.send_media_broadcast_start(self.playlist[self.inowplay]['type'],
                                                    self.playlist[self.inowplay]['video_id'])
                self.media_event_timer(self.playlist[self.inowplay]['video_time'])
                self.inowplay += 1

    # TODO: Handle pause and resume commands in media_event_timer or
    #       current_media_time_point/cancel_media_event_timer procedures(?)
    def media_event_timer(self, video_time):
        video_time_in_seconds = video_time / 1000
        # The next line should be where ever send_media_broadcast_start is called.
        # For now ill leave it here as it doesn't seem to cause any problems.
        # However if a tune gets paused, then current_media_time_point will return a wrong time
        # this could affect user joining the room and therefor it should be fixed.
        self.media_start_time = int(pinylib.time.time() * 1000)
        self.media_timer_thread = threading.Timer(video_time_in_seconds, self.media_event_handler)
        self.media_timer_thread.start()

    def current_media_time_point(self):
        if 'pause' in self.last_played_media:
            return self.last_played_media['pause']
        else:
            if self.media_timer_thread is not None:
                if self.media_timer_thread.is_alive():
                    ts_now = int(pinylib.time.time() * 1000)
                    elapsed_track_time = ts_now - self.media_start_time
                    return elapsed_track_time
                return 0
            return 0

    def cancel_media_event_timer(self):
        if self.media_timer_thread is not None:
            if self.media_timer_thread.is_alive():
                self.media_timer_thread.cancel()
                self.media_timer_thread = None
                return True
            return False
        return False

    def random_msg(self):
        if len(self.playlist) is not 0:
            if self.inowplay + 1 < len(self.playlist):
                next_video_title = self.playlist[self.inowplay]['video_title']
                next_video_time = self.to_human_time(self.playlist[self.inowplay]['video_time'])
                upnext = '*Next is:* (' + str(self.inowplay) + ') *' + next_video_title + '* ' + next_video_time
            inquee = len(self.playlist) - self.inowplay
            plstat = str(len(self.playlist)) + ' *items in the playlist.* ' + str(inquee) + ' *Still in queue.*'
        else:
            upnext = CONFIG['prefix'] + 'yt *(YouTube title, link or id) to add a YouTube to the playlist.'
            plstat = CONFIG['prefix'] + 'sc *(SoundCloud title or id)* to add a SoundCloud to the playlist.'

        messages = ['Reporting for duty..', 'Hello, is anyone here?', 'Awaiting command..', 'Observing behavior..',
                    upnext, plstat, '*I have been connected for:* ' + self.to_human_time(self.get_uptime()),
                    'Everyone alright?', 'What\'s everyone up to?',
                    'How is the weather where everyone is?', 'Why is everyone so quiet?',
                    'Anything in particular going on?' +
                    'Type: *' + CONFIG['prefix'] + 'help* for a list of commands',
                    'Anything interesting in the news lately?']

        return random.choice(messages)

    # Timed auto functions.
    def auto_msg_handler(self):
        if self.is_connected:
            self.send_bot_msg(self.random_msg())
        self.start_auto_msg_timer()

    def start_auto_msg_timer(self):
        threading.Timer(CONFIG['auto_message_interval'], self.auto_msg_handler).start()

    # Helper Methods.
    def get_privacy_settings(self):
        log.info('Parsing %s\'s privacy page. Proxy %s' % (self.account, self.proxy))
        self.privacy_settings = privacy_settings.TinychatPrivacyPage(self.proxy)
        self.privacy_settings.parse_privacy_settings()

    def get_uptime(self):
        up = int(pinylib.time.time() - self.init_time)
        return up * 1000

    @staticmethod
    def format_time_point(raw_time_point):
        if ':' in raw_time_point:
            time_point_components = raw_time_point.split(':')
        else:
            time_point_components = [raw_time_point]
        hour = 3600000
        minute = 60000
        second = 1000

        if len(time_point_components) < 3:
            if len(time_point_components) is 2:
                time_point_components.insert(0, '0')
            else:
                time_point_components.insert(0, '0')
                time_point_components.insert(0, '0')

        milliseconds = int(time_point_components[0]) * hour + int(time_point_components[1]) * minute \
                       + int(time_point_components[2]) * second
        return milliseconds

    @staticmethod
    def to_human_time(milliseconds):
        seconds = milliseconds / 1000

        m, s = divmod(seconds, 60)
        h, m = divmod(m, 60)
        d, h = divmod(h, 24)

        if d == 0 and h == 0:
            human_time = '%02d:%02d' % (m, s)
        elif d == 0:
            human_time = '%d:%02d:%02d' % (h, m, s)
        else:
            human_time = '%d Day(s) %d:%02d:%02d' % (d, h, m, s)
        return human_time

    def check_msg_for_bad_string(self, msg, pm=False):
        msg_words = msg.split(' ')
        bad_strings = pinylib.fh.file_reader(CONFIG['path'], CONFIG['badstrings'])
        if bad_strings is not None:
            for word in msg_words:
                if word in bad_strings:
                    self.send_ban_msg(self.user_obj.nick, self.user_obj.id)
                    if CONFIG['bsforgive']:
                        self.send_forgive_msg(self.user_obj.id)

    def connection_info(self):
        print('\nRoom location: %s, Room name: %s' % (self._embed_url, self.roomname))
        print('RTMP Info.:')
        print('IP: %s, PORT: %s, Proxy: %s, RTMP Url: %s, Playpath: %s' % (self._ip, self._port, self.proxy,
                                                                           self._tc_url, self._app))
        print('SWF (Desktop) Version: %s, SWF Url: %s' % (self._desktop_version, self._swf_url))
        print ('Tinychat Room Info.:')
        print('Nickname: %s, ID: %s, Account: %s' % (self.client_nick, self.client_id, self.account))
        print('SWF (Local) Version: %s, Type: %s, Greenroom: %s, Room password: %s, Room broadcasting password: %s' %
              (self._swf_version, self._roomtype, self.greenroom, self.room_pass, self.room_broadcast_pass))


def main():
    if CONFIG['auto_connect']:
        room_name = CONFIG['room']
        nickname = CONFIG['nick']
        room_password = CONFIG['room_password']
        login_account = CONFIG['account']
        login_password = CONFIG['account_password']

        if len(room_name) is 0:
            print('The ROOM name is empty in the configuration. You can configure this in '
            + str(CONFIG_FILE_NAME) + ' if you have \'auto_connect\' enabled.')
            # Exit to system safely whilst returning exit code 1.
            sys.exit(1)
    else:
        # Assign basic login variables.
        room_name = raw_input('Enter room name: ')

        while len(room_name) is 0:
            cls()
            print('Please enter a ROOM name to continue.')
            room_name = raw_input('Enter room name: ')

        room_password = pinylib.getpass.getpass('Enter room password (optional:password hidden): ')
        nickname = raw_input('Enter nick name (optional): ')
        login_account = raw_input('Login account (optional): ')
        login_password = pinylib.getpass.getpass('Login password (optional:password hidden): ')

    client = TinychatBot(room_name, nick=nickname, account=login_account,
                         password=login_password, room_pass=room_password)
    t = threading.Thread(target=client.prepare_connect)
    t.daemon = True
    t.start()

    while not client.is_connected:
        pinylib.time.sleep(1)
    threading.Thread(target=client.alive)
    client.console_write(pinylib.COLOR['white'], 'Started alive management.')
    threading.Thread(target=client.send_ping_request)
    client.console_write(pinylib.COLOR['white'], 'Started ping management.')

    while client.is_connected:
        chat_msg = raw_input()

        if chat_msg.lower() == '/q':
            client.disconnect()
            sys.exit(0)

        elif chat_msg.lower() == '/reconnect':
            client.reconnect()

        elif chat_msg.lower() == '/connection':
            client.connection_info()

        elif chat_msg.lower() == '/nick':
            new_nick = raw_input('\nNew bot nickname: ')
            client.client_nick = new_nick
            client.set_nick()

        elif chat_msg.lower() == '/pm':
            pm_nick = raw_input('\nNick to Private Message (PM): ')
            pm_msg = raw_input('\nEnter your message: ')
            client.send_private_msg(pm_msg, pm_nick)
        elif chat_msg.lower() == '/u':
            nick = raw_input('\nNick: ')
            msg = raw_input('\nMsg: ')
            client.send_undercover_msg(nick, msg)
        
        else:
            if CONFIG['console_msg_notice']:
                client.send_bot_msg(chat_msg, client.is_client_mod)
            else:
                client.send_chat_msg(chat_msg)
                client.console_write(pinylib.COLOR['cyan'], 'You:' + chat_msg)


if __name__ == '__main__':
    if CONFIG['debug_to_file']:
        formatter = '%(asctime)s : %(levelname)s : %(filename)s : %(lineno)d : %(funcName)s() : %(name)s : %(message)s'
        logging.basicConfig(filename=CONFIG['debug_file_name'], level=logging.DEBUG, format=formatter)
        log.info('Starting pinybot.py version: %s' % __version__)
    else:
        log.addHandler(logging.NullHandler())
    main()
